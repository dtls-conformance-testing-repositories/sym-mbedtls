//
// Created by hooman on 2020-12-18.
//
#include "stdint.h"
#include "records.h"
#include "stdio.h"
#include "memory.h"
#include <errno.h>
#include <stdlib.h>

Approach approach;
ExperimentType experimenttype;

int load_record(char *file_name, uint8_t *buf, uint16_t buff_size)
{
    FILE *f = fopen(file_name, "rb");
    if (f == NULL)
    {
        perror("Oops: ");
        return -1;
    }
    fread(buf, sizeof *buf, buff_size, f);
    if (ferror(f) != 0)
    {
        perror("Oops: ");
        return -1;
    }
    return 0;
}

static void parse_CH0(uint8_t *buf, CH0 *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6);
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3);
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3);
    memcpy(&rec->fragment_length, buf + 22, 3);
    memcpy(&rec->handshake_version, buf + 25, 2);
    memcpy(&rec->random, buf + 27, 32);
    rec->session_id_length = buf[59];
    rec->cookie_length = buf[60];
    memcpy(&rec->cipher_suite_length, buf + 61, 2);
#if TinyDTLS
    memcpy(&rec->cipher_suites, buf + 63, 2);
    rec->compression_length = buf[65];
    rec->compression_method = buf[66];
#elif MbedTLS
    memcpy(&rec->cipher_suites, buf + 63, 4);
    rec->compression_length = buf[67];
    rec->compression_method = buf[68];
    memcpy(&rec->extension_length, buf + 69, 2);
    memcpy(&rec->extensions, buf + 71, 58);
#elif OpenSSL
    memcpy(&rec->cipher_suites, buf + 63, 4);
    rec->compression_length = buf[67];
    rec->compression_method = buf[68];
    memcpy(&rec->extension_length, buf + 69, 2);
    memcpy(&rec->extensions, buf + 71, 70);
#elif OldOpenSSL
    memcpy(&rec->cipher_suites, buf + 63, 4);
    rec->compression_length = buf[67];
    rec->compression_method = buf[68];
    memcpy(&rec->extension_length, buf + 69, 2);
    memcpy(&rec->extensions, buf + 71, 5);
#endif
}
static void parse_CH2(uint8_t *buf, CH2 *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6); //Note
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3); //Note
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3); //Note
    memcpy(&rec->fragment_length, buf + 22, 3); //Note
    memcpy(&rec->handshake_version, buf + 25, 2);
    memcpy(&rec->random, buf + 27, 32); //Note
    rec->session_id_length = buf[59];
    rec->cookie_length = buf[60];
#if TinyDTLS
    memcpy(&rec->cookie, buf + 61, 16);
    memcpy(&rec->cipher_suite_length, buf + 77, 2);
    memcpy(&rec->cipher_suites, buf + 79, 2);
    rec->compression_length = buf[81];
    rec->compression_method = buf[82];
#elif MbedTLS
    memcpy(&rec->cookie, buf + 61, 32);
    memcpy(&rec->cipher_suite_length, buf + 93, 2);
    memcpy(&rec->cipher_suites, buf + 95, 4);
    rec->compression_length = buf[99];
    rec->compression_method = buf[100];
    memcpy(&rec->extension_length, buf + 101, 2);
    memcpy(&rec->extensions, buf + 103, 58);
#elif OpenSSL
    memcpy(&rec->cookie, buf + 61, 20);
    memcpy(&rec->cipher_suite_length, buf + 81, 2);
    memcpy(&rec->cipher_suites, buf + 83, 4);
    rec->compression_length = buf[87];
    rec->compression_method = buf[88];
    memcpy(&rec->extension_length, buf + 89, 2);
    memcpy(&rec->extensions, buf + 91, 70);
#elif OldOpenSSL
    memcpy(&rec->cookie, buf + 61, 20);
    memcpy(&rec->cipher_suite_length, buf + 81, 2);
    memcpy(&rec->cipher_suites, buf + 83, 4);
    rec->compression_length = buf[87];
    rec->compression_method = buf[88];
    memcpy(&rec->extension_length, buf + 89, 2);
    memcpy(&rec->extensions, buf + 91, 5);
#endif
}

static void parse_unfrag_CKE(uint8_t *buf, CKE *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6);
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3);
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3);
    memcpy(&rec->fragment_length, buf + 22, 3);
    memcpy(&rec->client_identity, buf + 25, 17);
}

static void parse_frag_CKE(uint8_t *buf, CKEFRAG *rec)
{
    rec->content_typef1 = buf[0];
    memcpy(&rec->record_versionf1, buf + 1, 2);
    memcpy(&rec->epochf1, buf + 3, 2);
    memcpy(&rec->sequence_numberf1, buf + 5, 6);
    memcpy(&rec->record_lengthf1, buf + 11, 2);
    rec->handshake_typef1 = buf[13];
    memcpy(&rec->handshake_lengthf1, buf + 14, 3);
    memcpy(&rec->message_sequencef1, buf + 17, 2);
    memcpy(&rec->fragment_offsetf1, buf + 19, 3);
    memcpy(&rec->fragment_lengthf1, buf + 22, 3);
    memcpy(&rec->client_identityf1, buf + 25, 13);

    rec->content_typef2 = buf[38];
    memcpy(&rec->record_versionf2, buf + 39, 2);
    memcpy(&rec->epochf2, buf + 41, 2);
    memcpy(&rec->sequence_numberf2, buf + 43, 6);
    memcpy(&rec->record_lengthf2, buf + 49, 2);
    rec->handshake_typef2 = buf[51];
    memcpy(&rec->handshake_lengthf2, buf + 52, 3);
    memcpy(&rec->message_sequencef2, buf + 55, 2);
    memcpy(&rec->fragment_offsetf2, buf + 57, 3);
    memcpy(&rec->fragment_lengthf2, buf + 60, 3);
    memcpy(&rec->client_identityf2, buf + 63, 4);
}

static void parse_fragf1_CKE(uint8_t *buf, CKEf1 *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6);
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3);
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3);
    memcpy(&rec->fragment_length, buf + 22, 3);
    memcpy(&rec->client_identity, buf + 25, 13);
}

static void parse_fragf2_CKE(uint8_t *buf, CKEf2 *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6);
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3);
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3);
    memcpy(&rec->fragment_length, buf + 22, 3);
    memcpy(&rec->client_identity, buf + 25, 4);
}

static void parse_HVR(uint8_t *buf, HVR *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6); //Note
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3); //Note
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3); //Note
    memcpy(&rec->fragment_length, buf + 22, 3); //Note
    memcpy(&rec->handshake_version, buf + 25, 2);
    rec->cookie_length = buf[27];
#if TinyDTLS
    memcpy(&rec->cookie, buf + 28, 16);
#elif MbedTLS
    memcpy(&rec->cookie, buf + 28, 32);
#elif OpenSSL | OldOpenSSL
    memcpy(&rec->cookie, buf + 28, 20);
#endif
}

static void parse_unfrag_SH(uint8_t *buf, SH *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6); //Note
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3); //Note
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3); //Note
    memcpy(&rec->fragment_length, buf + 22, 3); //Note
    memcpy(&rec->handshake_version, buf + 25, 2);
    memcpy(&rec->random, buf + 27, 32);
    rec->session_id_length = buf[59];
#if TinyDTLS
    memcpy(&rec->cipher_suite, buf + 60, 2);
    rec->compression_method = buf[62];
#elif MbedTLS
    memcpy(&rec->session_id, buf + 60, 32);
    memcpy(&rec->cipher_suite, buf + 92, 2);
    rec->compression_method = buf[94];
    memcpy(&rec->extension_length, buf + 95, 2);
    memcpy(&rec->extensions, buf + 97, 9);
#elif OpenSSL
    memcpy(&rec->session_id, buf + 60, 32);
    memcpy(&rec->cipher_suite, buf + 92, 2);
    rec->compression_method = buf[94];
    memcpy(&rec->extension_length, buf + 95, 2);
    memcpy(&rec->extensions, buf + 97, 13);
#elif OldOpenSSL
    memcpy(&rec->session_id, buf + 60, 32);
    memcpy(&rec->cipher_suite, buf + 92, 2);
    rec->compression_method = buf[94];
    memcpy(&rec->extension_length, buf + 95, 2);
    memcpy(&rec->extensions, buf + 97, 10);
#endif
}

static void parse_frag_SH(uint8_t *buf, SHFRAG *rec)
{
    rec->content_typef1 = buf[0];
    memcpy(&rec->record_versionf1, buf + 1, 2);
    memcpy(&rec->epochf1, buf + 3, 2);
    memcpy(&rec->sequence_numberf1, buf + 5, 6); //Note
    memcpy(&rec->record_lengthf1, buf + 11, 2);
    rec->handshake_typef1 = buf[13];
    memcpy(&rec->handshake_lengthf1, buf + 14, 3); //Note
    memcpy(&rec->message_sequencef1, buf + 17, 2);
    memcpy(&rec->fragment_offsetf1, buf + 19, 3); //Note
    memcpy(&rec->fragment_lengthf1, buf + 22, 3); //Note
    memcpy(&rec->handshake_version, buf + 25, 2);
    memcpy(&rec->random, buf + 27, 32);
    rec->content_typef2 = buf[59];
    memcpy(&rec->record_versionf2, buf + 60, 2);
    memcpy(&rec->epochf2, buf + 62, 2);
    memcpy(&rec->sequence_numberf2, buf + 64, 6);
    memcpy(&rec->record_lengthf2, buf + 70, 2);
    rec->handshake_typef2 = buf[72];
    memcpy(&rec->handshake_lengthf2, buf + 73, 3);
    memcpy(&rec->message_sequencef2, buf + 76, 2);
    memcpy(&rec->fragment_offsetf2, buf + 78, 3);
    memcpy(&rec->fragment_lengthf2, buf + 81, 3);
    rec->session_id_length = buf[84];
#if TinyDTLS
    memcpy(&rec->cipher_suite, buf + 85, 2);
    rec->compression_method = buf[87];
#elif MbedTLS
    memcpy(&rec->session_id, buf + 85, 32);
    memcpy(&rec->cipher_suite, buf + 117, 2);
    rec->compression_method = buf[119];
    memcpy(&rec->extension_length, buf + 120, 2);
    memcpy(&rec->extensions, buf + 122, 9);
#elif OpenSSL
    memcpy(&rec->session_id, buf + 85, 32);
    memcpy(&rec->cipher_suite, buf + 117, 2);
    rec->compression_method = buf[119];
    memcpy(&rec->extension_length, buf + 120, 2);
    memcpy(&rec->extensions, buf + 122, 13);
#elif OldOpenSSL
    memcpy(&rec->session_id, buf + 85, 32);
    memcpy(&rec->cipher_suite, buf + 117, 2);
    rec->compression_method = buf[119];
    memcpy(&rec->extension_length, buf + 120, 2);
    memcpy(&rec->extensions, buf + 122, 10);
#endif
}

static void parse_fragf1_SH(uint8_t *buf, SHf1 *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6); //Note
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3); //Note
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3); //Note
    memcpy(&rec->fragment_length, buf + 22, 3); //Note
    memcpy(&rec->handshake_version, buf + 25, 2);
    memcpy(&rec->random, buf + 27, 32);
}

static void parse_fragf2_SH(uint8_t *buf, SHf2 *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6); //Note
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3); //Note
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3); //Note
    memcpy(&rec->fragment_length, buf + 22, 3); //Note
    rec->session_id_length = buf[25];
    memcpy(&rec->cipher_suite, buf + 26, 2);
    rec->compression_method = buf[28];
}

static void parse_SHD(uint8_t *buf, SHD *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6); //Note
    memcpy(&rec->record_length, buf + 11, 2);
    rec->handshake_type = buf[13];
    memcpy(&rec->handshake_length, buf + 14, 3); //Note
    memcpy(&rec->message_sequence, buf + 17, 2);
    memcpy(&rec->fragment_offset, buf + 19, 3); //Note
    memcpy(&rec->fragment_length, buf + 22, 3); //Note
}
static void parse_CCS(uint8_t *buf, CCS *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6); //Note
    memcpy(&rec->record_length, buf + 11, 2);
    rec->ccs_msg = buf[13];
}
static void parse_FIN(uint8_t *buf, FIN *rec)
{
    rec->content_type = buf[0];
    memcpy(&rec->record_version, buf + 1, 2);
    memcpy(&rec->epoch, buf + 3, 2);
    memcpy(&rec->sequence_number, buf + 5, 6); //Note
    memcpy(&rec->record_length, buf + 11, 2);
#if TinyDTLS
    memcpy(&rec->encrypted_content, buf + 13, 40);
#elif MbedTLS
    memcpy(&rec->encrypted_content, buf + 13, 48);
#elif OpenSSL
    memcpy(&rec->encrypted_content, buf + 13, 68);
#elif OldOpenSSL
    memcpy(&rec->encrypted_content, buf + 13, 64);
#endif
}

void parse_record(uint8_t *buf, void *rec)
{
    switch (buf[0])
    {

    case Handshake_REC:
        switch (buf[13])
        {
        case Client_Hello_MSG:
            if (buf[60] == 0)
            {
                parse_CH0(buf, rec);
                break;
            }
            else
            {
                parse_CH2(buf, rec);
                break;
            }
        case Client_Key_Exchange_MSG:
            if (buf[16] != buf[24])
            {
#if TinyDTLS
                if (buf[21] == 0)
                {
                    parse_fragf1_CKE(buf, rec);
                }                    
                else
                {
                    parse_fragf2_CKE(buf, rec);
                }                    
#else
                parse_frag_CKE(buf, rec);
#endif
            }
            else
            {
                parse_unfrag_CKE(buf, rec);
            }
                
            break;
        case Hello_Verify_Request_MSG:
            parse_HVR(buf, rec);
            break;
        case Server_Hello_MSG:
      
            if (buf[16] != buf[24])
            {
#if TinyDTLS    
                if (buf[21] == 0)
                {
                    parse_fragf1_SH(buf, rec);
                }
                else
                {
                    parse_fragf2_SH(buf, rec);
                }
#else
                parse_frag_SH(buf, rec);
#endif                
            }                
            else
            {
                parse_unfrag_SH(buf, rec);
            }                
            break;
        case Server_Hello_Done_MSG:
            parse_SHD(buf, rec);
            break;
        case FINISHED_MSG:
            parse_FIN(buf, rec);
            break;
        case CFINISHED_MSG:
            parse_FIN(buf, rec);
            break;
        case SFINISHED_MSG:
            parse_FIN(buf, rec);
            break;
        case OCFINISHED_MSG:
            parse_FIN(buf, rec);
            break;
        case OSFINISHED_MSG:
            parse_FIN(buf, rec);
            break;
        }
        break;
    case Change_Cipher_Spec_REC:
        parse_CCS(buf, rec);
        break;
    }
}

void write_record_to_file(char *name, uint8_t *buf, size_t size)
{
    FILE *f = fopen(name, "wb");
    fwrite(buf, 1, size, f);
}

void dump_record(const void *buf, size_t size)
{
    const unsigned char *byte;
    for (byte = buf; size--; ++byte)
    {
        printf("%02X", *byte);
    }
    putchar('\n');
}
