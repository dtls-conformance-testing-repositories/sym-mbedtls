//
// Created by hooman on 2020-12-17.
//
#include "stdint.h"
#include "memory.h"

#ifndef RECORDS_RECORDS_H
#define RECORDS_RECORDS_H

#define ISSYM 1

#define TinyDTLS 0
#define MbedTLS 1
#define OpenSSL 0
#define OldOpenSSL 0

// Record Size
#if TinyDTLS
#define CH0_size 67
#define CH2_size 83
#define CKE_unfrag_size 42
#define CKE_frag1_size 38
#define CKE_frag2_size 29
#define CCS_size 14
#define FIN_size 53
#define HVR_size 44
#define SH_unfrag_size 63
#define SH_frag1_size 59
#define SH_frag2_size 29
#define SHD_size 25

#elif MbedTLS
#define CH0_size 129
#define CH2_size 161
#define CKE_unfrag_size 42
#define CKE_frag_size 67
#define CCS_size 14
#define FIN_size 61
#define HVR_size 60
#define SH_unfrag_size 106
#define SH_frag_size 131
#define SHD_size 25

#elif OpenSSL
#define CH0_size 141
#define CH2_size 161
#define CKE_unfrag_size 42
#define CKE_frag_size 67
#define CCS_size 14
#define FIN_size 81
#define HVR_size 48
#define SH_unfrag_size 110
#define SH_frag_size 135
#define SHD_size 25

#elif OldOpenSSL
#define CH0_size 76
#define CH2_size 96
#define CKE_unfrag_size 42
#define CKE_frag_size 67
#define CCS_size 14
#define FIN_size 77
#define HVR_size 48
#define SH_unfrag_size 107
#define SH_frag_size 132
#define SHD_size 25
#endif

// Record Types
#define Handshake_REC 22
#define Change_Cipher_Spec_REC 20
#define Client_Hello_MSG 1
#define Server_Hello_MSG 2
#define Hello_Verify_Request_MSG 3
#define Server_Hello_Done_MSG 14
#define Client_Key_Exchange_MSG 16


#define FINISHED_MSG 0
#define CFINISHED_MSG 220
#define SFINISHED_MSG 210
#define OCFINISHED_MSG 18
#define OSFINISHED_MSG 193

//

typedef struct __attribute__((packed)){
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    uint8_t handshake_version[2];
    char random[32];
    char session_id_length;
    char cookie_length;
    char cipher_suite_length[2];
#if TinyDTLS    
    char cipher_suites[2]; 
#elif MbedTLS | OpenSSL | OldOpenSSL
    char cipher_suites[4];    
#endif
    char compression_length;
    char compression_method;
#if MbedTLS
    char extension_length[2];
    char extensions[58];
#elif OpenSSL
    char extension_length[2];
    char extensions[70];
#elif OldOpenSSL
    char extension_length[2];
    char extensions[5];
#endif

}CH0;

typedef struct __attribute__((packed)){
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    uint8_t handshake_version[2];
    char random[32];
    char session_id_length;
    char cookie_length;
#if TinyDTLS
    char cookie[16];    
#elif MbedTLS
    char cookie[32];
#elif OpenSSL | OldOpenSSL
    char cookie[20];
#endif
    char cipher_suite_length[2];
#if TinyDTLS
    char cipher_suites[2]; 
#elif MbedTLS | OpenSSL | OldOpenSSL
    char cipher_suites[4]; 
#endif
    char compression_length;
    char compression_method;
#if MbedTLS
    char extension_length[2];
    char extensions[58];
#elif OpenSSL
    char extension_length[2];
    char extensions[70];
#elif OldOpenSSL
    char extension_length[2];
    char extensions[5];
#endif

}CH2;

typedef struct __attribute__((packed)){
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    char client_identity[17];

}CKE;

typedef struct __attribute__((packed)){
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    char client_identity[13];
}CKEf1;

typedef struct __attribute__((packed)){
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    char client_identity[4];
}CKEf2;

typedef struct __attribute__((packed)){
    char content_typef1;
    uint8_t record_versionf1[2];
    char epochf1[2];
    char sequence_numberf1[6];
    char record_lengthf1[2];
    uint8_t handshake_typef1;
    char handshake_lengthf1[3];
    char message_sequencef1[2];
    char fragment_offsetf1[3];
    char fragment_lengthf1[3];
    char client_identityf1[13];

    char content_typef2;
    uint8_t record_versionf2[2];
    char epochf2[2];
    char sequence_numberf2[6];
    char record_lengthf2[2];
    uint8_t handshake_typef2;
    char handshake_lengthf2[3];
    char message_sequencef2[2];
    char fragment_offsetf2[3];
    char fragment_lengthf2[3];
    char client_identityf2[4];
}CKEFRAG;

typedef struct __attribute__((packed)){
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    char ccs_msg;
}CCS;

typedef struct __attribute__((packed)){
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
#if TinyDTLS
    char encrypted_content[40];
#elif MbedTLS
    char encrypted_content[48];
#elif OpenSSL
    char encrypted_content[68];
#elif OldOpenSSL
    char encrypted_content[64];
#endif
}FIN;

typedef struct __attribute__((packed)){
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    uint8_t handshake_version[2];
    char cookie_length;
#if TinyDTLS
    char cookie[16];
#elif MbedTLS
    char cookie[32];
#elif OpenSSL | OldOpenSSL
    char cookie[20];
#endif    
}HVR;

typedef struct __attribute__((packed))
{
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    uint8_t handshake_version[2];
    char random[32];
    char session_id_length;
#if MbedTLS | OpenSSL | OldOpenSSL
    char session_id[32];
#endif
    char cipher_suite[2]; 
    char compression_method;
#if MbedTLS    
    char extension_length[2];
    char extensions[9];
#elif OpenSSL
    char extension_length[2];
    char extensions[13];
#elif OldOpenSSL
    char extension_length[2];
    char extensions[10];
#endif    
}SH;

typedef struct __attribute__((packed))
{
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    uint8_t handshake_version[2];
    char random[32]; 
}SHf1;

typedef struct __attribute__((packed))
{
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
    char session_id_length;
    char cipher_suite[2]; 
    char compression_method;   
}SHf2;

typedef struct __attribute__((packed))
{
    char content_typef1;
    uint8_t record_versionf1[2];
    char epochf1[2];
    char sequence_numberf1[6];
    char record_lengthf1[2];
    uint8_t handshake_typef1;
    char handshake_lengthf1[3];
    char message_sequencef1[2];
    char fragment_offsetf1[3];
    char fragment_lengthf1[3];
    uint8_t handshake_version[2];
    char random[32];
    char content_typef2;
    uint8_t record_versionf2[2];
    char epochf2[2];
    char sequence_numberf2[6];
    char record_lengthf2[2];
    uint8_t handshake_typef2;
    char handshake_lengthf2[3];
    char message_sequencef2[2];
    char fragment_offsetf2[3];
    char fragment_lengthf2[3];
    char session_id_length;
#if MbedTLS | OpenSSL | OldOpenSSL
    char session_id[32];
#endif
    char cipher_suite[2]; 
    char compression_method;
#if MbedTLS    
    char extension_length[2];
    char extensions[9];
#elif OpenSSL
    char extension_length[2];
    char extensions[13];
#elif OldOpenSSL
    char extension_length[2];
    char extensions[10];    
#endif    
}SHFRAG;

typedef struct __attribute__((packed))
{
    char content_type;
    uint8_t record_version[2];
    char epoch[2];
    char sequence_number[6];
    char record_length[2];
    uint8_t handshake_type;
    char handshake_length[3];
    char message_sequence[2];
    char fragment_offset[3];
    char fragment_length[3];
}SHD;


///////// Types to determine the experiment type

typedef enum {
    None,
    CONFORMANCE, 
    SYMBEX}Approach;

typedef enum {
    CLIENT, 
    SERVER} ExperimentType;

extern Approach approach;
extern ExperimentType experimenttype;

/////////


int load_record(char* filename, uint8_t *buf, uint16_t buff_size);
void parse_record(uint8_t *buf, void *rec);

void write_record_to_file(char *name, uint8_t *buf, size_t size);
void dump_record(const void *buf, size_t size);

#endif //RECORDS_RECORDS_H

