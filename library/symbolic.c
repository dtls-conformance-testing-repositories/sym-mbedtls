#include <stdint.h>
#include <klee/klee.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "records.h"
#include "symbolic.h"
#pragma clang diagnostic ignored "-Wunused-parameter"

#define OR |
#define AND &
#define NEGATION !
//////////////////////////////////////////////////////////////////////
static void makeEpochSymbolic(void *record, int rec_type)
{
  char epoch_CH0[2];
  char epoch_CH2[2];
  char epoch_CKE[2];
  char epoch_CCC[2];
  char epoch_HVR[2];
  char epoch_SH[2];
  char epoch_SHD[2];
  char epoch_SCC[2];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&epoch_CH0, 2, "CH0-Epoch");
    memcpy(&((CH0 *)(record))->epoch, epoch_CH0, 2);
    break;
  case 2:
    klee_make_symbolic(&epoch_CH2, 2, "CH2-Epoch");
    memcpy(&((CH2 *)(record))->epoch, epoch_CH2, 2);
    break;
  case 5:
    klee_make_symbolic(&epoch_CKE, 2, "CKE-Epoch");
    memcpy(&((CKE *)(record))->epoch, epoch_CKE, 2);
    break;
  case 6:
    klee_make_symbolic(&epoch_CCC, 2, "CCC-Epoch");
    memcpy(&((CCS *)(record))->epoch, epoch_CCC, 2);
    break;
  case 1:
    klee_make_symbolic(&epoch_HVR, 2, "HVR-Epoch");
    memcpy(&((HVR *)(record))->epoch, epoch_HVR, 2);
    break;
  case 3:
    klee_make_symbolic(&epoch_SH, 2, "SH-Epoch");
    memcpy(&((SH *)(record))->epoch, epoch_SH, 2);
    break;
  case 4:
    klee_make_symbolic(&epoch_SHD, 2, "SHD-Epoch");
    memcpy(&((SHD *)(record))->epoch, epoch_SHD, 2);
    break;
  case 8:
    klee_make_symbolic(&epoch_SCC, 2, "SCC-Epoch");
    memcpy(&((CCS *)(record))->epoch, epoch_SCC, 2);
    break;
  default:
    break;
  }
}

static void makeSequenceSymbolic(void *record, int rec_type)
{
  char sequence_CH0[6];
  char sequence_CH2[6];
  char sequene_CKE[6];
  char sequence_CCC[6];
  char sequence_HVR[6];
  char sequence_SH[6];
  char sequene_SHD[6];
  char sequence_SCC[6];
  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&sequence_CH0, 6, "CH0-RecordSequence");
    memcpy(&((CH0 *)(record))->sequence_number, sequence_CH0, 6);
    klee_assume((((CH0 *)(record))->sequence_number[0] == 0x00) AND(((CH0 *)(record))->sequence_number[1] == 0x00) AND(((CH0 *)(record))->sequence_number[2] == 0x00) AND(((CH0 *)(record))->sequence_number[3] == 0x00) AND(((CH0 *)(record))->sequence_number[4] == 0x00));
    break;
  case 2:
    klee_make_symbolic(&sequence_CH2, 6, "CH2-RecordSequence");
    memcpy(&((CH2 *)(record))->sequence_number, sequence_CH2, 6);
    klee_assume((((CH2 *)(record))->sequence_number[0] == 0x00) AND(((CH2 *)(record))->sequence_number[1] == 0x00) AND(((CH2 *)(record))->sequence_number[2] == 0x00) AND(((CH2 *)(record))->sequence_number[3] == 0x00) AND(((CH2 *)(record))->sequence_number[4] == 0x00));
    break;
  case 5:
    klee_make_symbolic(&sequene_CKE, 6, "CKE-RecordSequence");
    memcpy(&((CKE *)(record))->sequence_number, sequene_CKE, 6);
    klee_assume((((CKE *)(record))->sequence_number[0] == 0x00) AND(((CKE *)(record))->sequence_number[1] == 0x00) AND(((CKE *)(record))->sequence_number[2] == 0x00) AND(((CKE *)(record))->sequence_number[3] == 0x00) AND(((CKE *)(record))->sequence_number[4] == 0x00));
    break;
  case 6:
    klee_make_symbolic(&sequence_CCC, 6, "CCC-RecordSequence");
    memcpy(&((CCS *)(record))->sequence_number, sequence_CCC, 6);
    klee_assume((((CCS *)(record))->sequence_number[0] == 0x00) AND(((CCS *)(record))->sequence_number[1] == 0x00) AND(((CCS *)(record))->sequence_number[2] == 0x00) AND(((CCS *)(record))->sequence_number[3] == 0x00) AND(((CCS *)(record))->sequence_number[4] == 0x00));
    break;
  case 1:
    klee_make_symbolic(&sequence_HVR, 6, "HVR-RecordSequence");
    memcpy(&((HVR *)(record))->sequence_number, sequence_HVR, 6);
    klee_assume((((HVR *)(record))->sequence_number[0] == 0x00) AND(((HVR *)(record))->sequence_number[1] == 0x00) AND(((HVR *)(record))->sequence_number[2] == 0x00) AND(((HVR *)(record))->sequence_number[3] == 0x00) AND(((HVR *)(record))->sequence_number[4] == 0x00));
    break;
  case 3:
    klee_make_symbolic(&sequence_SH, 6, "SH-RecordSequence");
    memcpy(&((SH *)(record))->sequence_number, sequence_SH, 6);
    klee_assume((((SH *)(record))->sequence_number[0] == 0x00) AND(((SH *)(record))->sequence_number[1] == 0x00) AND(((SH *)(record))->sequence_number[2] == 0x00) AND(((SH *)(record))->sequence_number[3] == 0x00) AND(((SH *)(record))->sequence_number[4] == 0x00));
    break;
  case 4:
    klee_make_symbolic(&sequene_SHD, 6, "SHD-RecordSequence");
    memcpy(&((SHD *)(record))->sequence_number, sequene_SHD, 6);
    klee_assume((((SHD *)(record))->sequence_number[0] == 0x00) AND(((SHD *)(record))->sequence_number[1] == 0x00) AND(((SHD *)(record))->sequence_number[2] == 0x00) AND(((SHD *)(record))->sequence_number[3] == 0x00) AND(((SHD *)(record))->sequence_number[4] == 0x00));
    break;
  case 8:
    klee_make_symbolic(&sequence_SCC, 6, "SCC-RecordSequence");
    memcpy(&((CCS *)(record))->sequence_number, sequence_SCC, 6);
    klee_assume((((CCS *)(record))->sequence_number[0] == 0x00) AND(((CCS *)(record))->sequence_number[1] == 0x00) AND(((CCS *)(record))->sequence_number[2] == 0x00) AND(((CCS *)(record))->sequence_number[3] == 0x00) AND(((CCS *)(record))->sequence_number[4] == 0x00));
    break;
  default:
    break;
  }
}

static void makeMessageSequenceSymbolic(void *record, int rec_type)
{
  char mSeq_CH0[2];
  char mSeq_CH2[2];
  char mSeq_CKE[2];
  char mSeq_HVR[2];
  char mSeq_SH[2];
  char mSeq_SHD[2];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&mSeq_CH0, 2, "CH0-MessageSequence");
    memcpy(&((CH0 *)(record))->message_sequence, mSeq_CH0, 2);
    klee_assume(((CH0 *)(record))->message_sequence[0] == 0x00);
    break;
  case 2:
    klee_make_symbolic(&mSeq_CH2, 2, "CH2-MessageSequence");
    memcpy(&((CH2 *)(record))->message_sequence, mSeq_CH2, 2);
    klee_assume(((CH2 *)(record))->message_sequence[0] == 0x00);
    break;
  case 5:
    klee_make_symbolic(&mSeq_CKE, 2, "CKE-MessageSequence");
    memcpy(&((CKE *)(record))->message_sequence, mSeq_CKE, 2);
    klee_assume(((CKE *)(record))->message_sequence[0] == 0x00);
    break;
  case 1:
    klee_make_symbolic(&mSeq_HVR, 2, "HVR-MessageSequence");
    memcpy(&((HVR *)(record))->message_sequence, mSeq_HVR, 2);
    klee_assume(((HVR *)(record))->message_sequence[0] == 0x00);
    break;
  case 3:
    klee_make_symbolic(&mSeq_SH, 2, "SH-MessageSequence");
    memcpy(&((SH *)(record))->message_sequence, mSeq_SH, 2);
    klee_assume(((SH *)(record))->message_sequence[0] == 0x00);
    break;
  case 4:
    klee_make_symbolic(&mSeq_SHD, 2, "SHD-MessageSequence");
    memcpy(&((SHD *)(record))->message_sequence, mSeq_SHD, 2);
    klee_assume(((SHD *)(record))->message_sequence[0] == 0x00);
    break;
  default:
    break;
  }
}

static void makeFragmentOffsetSymbolicNofrag(void *record, int rec_type)
{
  char fragmentoffset_CH0[3];
  char fragmentoffset_CH2[3];
  char fragmentoffset_CKE[3];
  char fragmentoffset_HVR[3];
  char fragmentoffset_SH[3];
  char fragmentoffset_SHD[3];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&fragmentoffset_CH0, 3, "CH0-FragmentOffset");
    memcpy(&((CH0 *)(record))->fragment_offset, fragmentoffset_CH0, 3);
    klee_assume((((CH0 *)(record))->fragment_offset[0] == 0x00) AND(((CH0 *)(record))->fragment_offset[1] == 0x00));
    break;
  case 2:
    klee_make_symbolic(&fragmentoffset_CH2, 3, "CH2-FragmentOffset");
    memcpy(&((CH2 *)(record))->fragment_offset, fragmentoffset_CH2, 3);
    klee_assume((((CH2 *)(record))->fragment_offset[0] == 0x00) AND(((CH2 *)(record))->fragment_offset[1] == 0x00));
    break;
  case 5:
    klee_make_symbolic(&fragmentoffset_CKE, 3, "CKE-FragmentOffset");
    memcpy(&((CKE *)(record))->fragment_offset, fragmentoffset_CKE, 3);
    klee_assume((((CKE *)(record))->fragment_offset[0] == 0x00) AND(((CKE *)(record))->fragment_offset[1] == 0x00));
    break;
  case 1:
    klee_make_symbolic(&fragmentoffset_HVR, 3, "HVR-FragmentOffset");
    memcpy(&((HVR *)(record))->fragment_offset, fragmentoffset_HVR, 3);
    klee_assume((((HVR *)(record))->fragment_offset[0] == 0x00) AND(((HVR *)(record))->fragment_offset[1] == 0x00));
    break;
  case 3:
    klee_make_symbolic(&fragmentoffset_SH, 3, "SH-FragmentOffset");
    memcpy(&((SH *)(record))->fragment_offset, fragmentoffset_SH, 3);
    klee_assume((((SH *)(record))->fragment_offset[0] == 0x00) AND(((SH *)(record))->fragment_offset[1] == 0x00));
    break;
  case 4:
    klee_make_symbolic(&fragmentoffset_SHD, 3, "SHD-FragmentOffset");
    memcpy(&((SHD *)(record))->fragment_offset, fragmentoffset_SHD, 3);
    klee_assume((((SHD *)(record))->fragment_offset[0] == 0x00) AND(((SHD *)(record))->fragment_offset[1] == 0x00));
    break;
  default:
    break;
  }
}

static void makeRecordLenSymbolic(void *record, int rec_type)
{
  char recordlength_CH0[2];
  char recordlength_CH2[2];
  char recordlength_CKE[2];
  char recordlength_CCC[2];
  char recordlength_HVR[2];
  char recordlength_SH[2];
  char recordlength_SHD[2];
  char recordlength_SCC[2];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&recordlength_CH0, 2, "CH0-RecordLength");
    memcpy(&((CH0 *)(record))->record_length, recordlength_CH0, 2);
    break;
  case 2:
    klee_make_symbolic(&recordlength_CH2, 2, "CH2-RecordLength");
    memcpy(&((CH2 *)(record))->record_length, recordlength_CH2, 2);
    break;
  case 5:
    klee_make_symbolic(&recordlength_CKE, 2, "CKE-RecordLength");
    memcpy(&((CKE *)(record))->record_length, recordlength_CKE, 2);
    break;
  case 6:
    klee_make_symbolic(&recordlength_CCC, 2, "CCC-RecordLength");
    memcpy(&((CCS *)(record))->record_length, recordlength_CCC, 2);
    break;
  case 1:
    klee_make_symbolic(&recordlength_HVR, 2, "HVR-RecordLength");
    memcpy(&((HVR *)(record))->record_length, recordlength_HVR, 2);
    break;
  case 3:
    klee_make_symbolic(&recordlength_SH, 2, "SH-RecordLength");
    memcpy(&((SH *)(record))->record_length, recordlength_SH, 2);
    break;
  case 4:
    klee_make_symbolic(&recordlength_SHD, 2, "SHD-RecordLength");
    memcpy(&((SHD *)(record))->record_length, recordlength_SHD, 2);
    break;
  case 8:
    klee_make_symbolic(&recordlength_SCC, 2, "SCC-RecordLength");
    memcpy(&((CCS *)(record))->record_length, recordlength_SCC, 2);
    break;
  default:
    break;
  }
}

static void makeFragmentLenSymbolicNoFrag(void *record, int rec_type)
{
  char fragmentlength_CH0[3];
  char fragmentlength_CH2[3];
  char fragmentlength_CKE[3];
  char fragmentlength_HVR[3];
  char fragmentlength_SH[3];
  char fragmentlength_SHD[3];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&fragmentlength_CH0, 3, "CH0-FragmentLength0");
    memcpy(&((CH0 *)(record))->fragment_length, fragmentlength_CH0, 3);
    break;
  case 2:
    klee_make_symbolic(&fragmentlength_CH2, 3, "CH2-FragmentLength0");
    memcpy(&((CH2 *)(record))->fragment_length, fragmentlength_CH2, 3);
    break;
  case 5:
    klee_make_symbolic(&fragmentlength_CKE, 3, "CKE-FragmentLength0");
    memcpy(&((CKE *)(record))->fragment_length, fragmentlength_CKE, 3);
    break;
  case 1:
    klee_make_symbolic(&fragmentlength_HVR, 3, "HVR-FragmentLength0");
    memcpy(&((HVR *)(record))->fragment_length, fragmentlength_HVR, 3);
    break;
  case 3:
    klee_make_symbolic(&fragmentlength_SH, 3, "SH-FragmentLength0");
    memcpy(&((SH *)(record))->fragment_length, fragmentlength_SH, 3);
    break;
  case 4:
    klee_make_symbolic(&fragmentlength_SHD, 3, "SHD-FragmentLength0");
    memcpy(&((SHD *)(record))->fragment_length, fragmentlength_SHD, 3);
    break;
  default:
    break;
  }
}

static void makeMessageLenSymbolicNoFrag(void *record, int rec_type)
{
  char handshakelength_CH0[3];
  char handshakelength_CH2[3];
  char handshakelength_CKE[3];
  char handshakelength_SH[3];
  char handshakelength_SHD[3];
  char handshakelength_HVR[3];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&handshakelength_CH0, 3, "CH0-HandshakeLength0");
    memcpy(&((CH0 *)(record))->handshake_length, handshakelength_CH0, 3);
    break;
  case 2:
    klee_make_symbolic(&handshakelength_CH2, 3, "CH2-HandshakeLength0");
    memcpy(&((CH2 *)(record))->handshake_length, handshakelength_CH2, 3);
    break;
  case 5:
    klee_make_symbolic(&handshakelength_CKE, 3, "CKE-HandshakeLength0");
    memcpy(&((CKE *)(record))->handshake_length, handshakelength_CKE, 3);
    break;
  case 1:
    klee_make_symbolic(&handshakelength_HVR, 3, "HVR-HandshakeLength0");
    memcpy(&((HVR *)(record))->handshake_length, handshakelength_HVR, 3);
    break;
  case 3:
    klee_make_symbolic(&handshakelength_SH, 3, "SH-HandshakeLength0");
    memcpy(&((SH *)(record))->handshake_length, handshakelength_SH, 3);
    break;
  case 4:
    klee_make_symbolic(&handshakelength_SHD, 3, "SHD-HandshakeLength0");
    memcpy(&((SHD *)(record))->handshake_length, handshakelength_SHD, 3);
    break;
  default:
    break;
  }
}

static void makeRecordVersionSymbolic(void *record, int rec_type)
{
  char recordversion_CH0[2];
  char recordversion_CH2[2];
  char recordversion_CKE[2];
  char recordversion_CCC[2];
  char recordversion_HVR[2];
  char recordversion_SH[2];
  char recordversion_SHD[2];
  char recordversion_SCC[2];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&recordversion_CH0, 2, "CH0-RecordVersion0");
    memcpy(&((CH0 *)(record))->record_version, recordversion_CH0, 2);
    break;
  case 2:
    klee_make_symbolic(&recordversion_CH2, 2, "CH2-RecordVersion0");
    memcpy(&((CH2 *)(record))->record_version, recordversion_CH2, 2);
    break;
  case 5:
    klee_make_symbolic(&recordversion_CKE, 2, "CKE-RecordVersion0");
    memcpy(&((CKE *)(record))->record_version, recordversion_CKE, 2);
    break;
  case 6:
    klee_make_symbolic(&recordversion_CCC, 2, "CCC-RecordVersion0");
    memcpy(&((CCS *)(record))->record_version, recordversion_CCC, 2);
    break;
  case 1:
    klee_make_symbolic(&recordversion_HVR, 2, "HVR-RecordVersion0");
    memcpy(&((HVR *)(record))->record_version, recordversion_HVR, 2);
    break;
  case 3:
    klee_make_symbolic(&recordversion_SH, 2, "SH-RecordVersion0");
    memcpy(&((SH *)(record))->record_version, recordversion_SH, 2);
    break;
  case 4:
    klee_make_symbolic(&recordversion_SHD, 2, "SHD-RecordVersion0");
    memcpy(&((SHD *)(record))->record_version, recordversion_SHD, 2);
    break;
  case 8:
    klee_make_symbolic(&recordversion_SCC, 2, "SCC-RecordVersion0");
    memcpy(&((CCS *)(record))->record_version, recordversion_SCC, 2);
    break;
  default:
    break;
  }
}

static void makeHandshakeVersionSymbolic(void *record, int rec_type)
{
  char handshakeversion_CH0[2];
  char handshakeversion_CH2[2];
  char handshakeversion_HVR[2];
  char handshakeversion_SH[2];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&handshakeversion_CH0, 2, "CH0-HandshakeVerion0");
    memcpy(&((CH0 *)(record))->handshake_version, handshakeversion_CH0, 2);
    break;
  case 2:
    klee_make_symbolic(&handshakeversion_CH2, 2, "CH2-HandshakeVerion0");
    memcpy(&((CH2 *)(record))->handshake_version, handshakeversion_CH2, 2);
    break;
  case 1:
    klee_make_symbolic(&handshakeversion_HVR, 2, "HVR-HandshakeVerion0");
    memcpy(&((HVR *)(record))->handshake_version, handshakeversion_HVR, 2);
    break;
  case 3:
    klee_make_symbolic(&handshakeversion_SH, 2, "SH-HandshakeVerion0");
    memcpy(&((SH *)(record))->handshake_version, handshakeversion_SH, 2);
    break;
  default:
    break;
  }
}

static void makeCookieLengthSymbolic(void *record, int rec_type)
{
  char Cookie_CH0;
  char Cookie_CH2;
  char Cookie_HVR;

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&Cookie_CH0, 1, "CH0-Cookie");
    ((CH0 *)record)->cookie_length = Cookie_CH0;
    break;
  case 2:
    klee_make_symbolic(&Cookie_CH2, 1, "CH2-Cookie");
    ((CH2 *)record)->cookie_length = Cookie_CH2;
    break;
  case 1:
    klee_make_symbolic(&Cookie_HVR, 1, "HVR-Cookie");
    ((HVR *)record)->cookie_length = Cookie_HVR;
    break;
  default:
    break;
  }
}

static void makeSessionIDLengthSymbolic(void *record, int rec_type)
{
  char sessionID_CH0;
  char sessionID_CH2;
  char sessionID_SH;

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&sessionID_CH0, 1, "CH0-sessionID");
    ((CH0 *)record)->session_id_length = sessionID_CH0;
    break;
  case 2:
    klee_make_symbolic(&sessionID_CH2, 1, "CH2-sessionID");
    ((CH2 *)record)->session_id_length = sessionID_CH2;
    break;
  case 3:
    klee_make_symbolic(&sessionID_SH, 1, "SH-sessionID");
    ((SH *)record)->session_id_length = sessionID_SH;
    break;
  default:
    break;
  }
}

static void makeContentTypeSybmolic(void *record, int rec_type)
{
  char contenttype_CH0;
  char contenttype_CH2;
  char contenttype_CKE;
  char contenttype_CCC;
  char contenttype_HVR;
  char contenttype_SH;
  char contenttype_SHD;
  char contenttype_SCC;

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&contenttype_CH0, 1, "CH0-ContentType");
    ((CH0 *)(record))->content_type = contenttype_CH0;
    break;
  case 2:
    klee_make_symbolic(&contenttype_CH2, 1, "CH2-ContentType");
    ((CH2 *)(record))->content_type = contenttype_CH2;
    break;
  case 5:
    klee_make_symbolic(&contenttype_CKE, 1, "CKE-ContentType");
    ((CKE *)(record))->content_type = contenttype_CKE;
    break;
  case 6:
    klee_make_symbolic(&contenttype_CCC, 1, "CCC-ContentType");
    ((CCS *)(record))->content_type = contenttype_CCC;
    break;
  case 1:
    klee_make_symbolic(&contenttype_HVR, 1, "HVR-ContentType");
    ((HVR *)(record))->content_type = contenttype_HVR;
    break;
  case 3:
    klee_make_symbolic(&contenttype_SH, 1, "SH-ContentType");
    ((SH *)(record))->content_type = contenttype_SH;
    break;
  case 4:
    klee_make_symbolic(&contenttype_SHD, 1, "SHD-ContentType");
    ((SHD *)(record))->content_type = contenttype_SHD;
    break;
  case 8:
    klee_make_symbolic(&contenttype_SCC, 1, "SCC-ContentType");
    ((CCS *)(record))->content_type = contenttype_SCC;
    break;
  default:
    break;
  }
}

static void makeFragmentOffsetSymbolicFrag(void *record, int rec_type)
{
  char fragmentoffset_CKEf1[3];
  char fragmentoffset_CKEf2[3];
  char fragmentoffset_SHf1[3];
  char fragmentoffset_SHf2[3];

  switch (rec_type)
  {
  case 5:
    klee_make_symbolic(&fragmentoffset_CKEf1, 3, "CKE-FragmentOffsetf1");
    klee_make_symbolic(&fragmentoffset_CKEf2, 3, "CKE-FragmentOffsetf2");
    memcpy(&((CKEFRAG *)(record))->fragment_offsetf1, fragmentoffset_CKEf1, 3);
    memcpy(&((CKEFRAG *)(record))->fragment_offsetf2, fragmentoffset_CKEf2, 3);
    break;
  case 51:
    klee_make_symbolic(&fragmentoffset_CKEf1, 3, "CKE-FragmentOffsetf1");
    memcpy(&((CKEf1 *)(record))->fragment_offset, fragmentoffset_CKEf1, 3);
    break;
  case 52:
    klee_make_symbolic(&fragmentoffset_CKEf2, 3, "CKE-FragmentOffsetf2");
    memcpy(&((CKEf2 *)(record))->fragment_offset, fragmentoffset_CKEf2, 3);
    break;
  case 3:
    klee_make_symbolic(&fragmentoffset_SHf1, 3, "SH-FragmentOffsetf1");
    klee_make_symbolic(&fragmentoffset_SHf2, 3, "SH-FragmentOffsetf2");
    memcpy(&((SHFRAG *)(record))->fragment_offsetf1, fragmentoffset_SHf1, 3);
    memcpy(&((SHFRAG *)(record))->fragment_offsetf2, fragmentoffset_SHf2, 3);
    break;
  case 31:
    klee_make_symbolic(&fragmentoffset_SHf1, 3, "SH-FragmentOffsetf1");
    memcpy(&((SHf1 *)(record))->fragment_offset, fragmentoffset_SHf1, 3);
    break;
  case 32:
    klee_make_symbolic(&fragmentoffset_SHf2, 3, "SH-FragmentOffsetf2");
    memcpy(&((SHf2 *)(record))->fragment_offset, fragmentoffset_SHf2, 3);
    break;
  default:
    break;
  }
}

static void makeFragmentLenSymbolicFrag(void *record, int rec_type)
{
  char fragmentlength_CKEf1[3];
  char fragmentlength_CKEf2[3];
  char fragmentlength_SHf1[3];
  char fragmentlength_SHf2[3];

  switch (rec_type)
  {
  case 5:
    klee_make_symbolic(&fragmentlength_CKEf1, 3, "CKEf1-FragmentLength0");
    memcpy(&((CKEFRAG *)(record))->fragment_lengthf1, fragmentlength_CKEf1, 3);
    //
    klee_make_symbolic(&fragmentlength_CKEf2, 3, "CKEf2-FragmentLength0");
    memcpy(&((CKEFRAG *)(record))->fragment_lengthf2, fragmentlength_CKEf2, 3);
    break;
  case 51:
    klee_make_symbolic(&fragmentlength_CKEf1, 3, "CKEf1-FragmentLength0");
    memcpy(&((CKEf1 *)(record))->fragment_length, fragmentlength_CKEf1, 3);
    break;
  case 52:
    klee_make_symbolic(&fragmentlength_CKEf2, 3, "CKEf2-FragmentLength0");
    memcpy(&((CKEf2 *)(record))->fragment_length, fragmentlength_CKEf2, 3);
    break;
  case 3:
    klee_make_symbolic(&fragmentlength_SHf1, 3, "SHf1-FragmentLength0");
    memcpy(&((SHFRAG *)(record))->fragment_lengthf1, fragmentlength_SHf1, 3);
    //
    klee_make_symbolic(&fragmentlength_SHf2, 3, "SHf2-FragmentLength0");
    memcpy(&((SHFRAG *)(record))->fragment_lengthf2, fragmentlength_SHf2, 3);
    break;
  case 31:
    klee_make_symbolic(&fragmentlength_SHf1, 3, "SHf1-FragmentLength0");
    memcpy(&((SHf1 *)(record))->fragment_length, fragmentlength_SHf1, 3);
    break;
  case 32:
    klee_make_symbolic(&fragmentlength_SHf2, 3, "SHf2-FragmentLength0");
    memcpy(&((SHf2 *)(record))->fragment_length, fragmentlength_SHf2, 3);
    break;
  default:
    break;
  }
}

static void makeMessageLenSymbolicFrag(void *record, int rec_type)
{
  char handshakelength_CKEf1[3];
  char handshakelength_CKEf2[3];
  char handshakelength_SHf1[3];
  char handshakelength_SHf2[3];
  switch (rec_type)
  {
  case 5:
    klee_make_symbolic(&handshakelength_CKEf1, 3, "CKEf1-HandshakeLength0");
    memcpy(&((CKEFRAG *)(record))->handshake_lengthf1, handshakelength_CKEf1, 3);
    //
    klee_make_symbolic(&handshakelength_CKEf2, 3, "CKEf2-HandshakeLength0");
    memcpy(&((CKEFRAG *)(record))->handshake_lengthf2, handshakelength_CKEf2, 3);
    break;
  case 51:
    klee_make_symbolic(&handshakelength_CKEf1, 3, "CKEf1-HandshakeLength0");
    memcpy(&((CKEf1 *)(record))->handshake_length, handshakelength_CKEf1, 3);
    break;
  case 52:
    klee_make_symbolic(&handshakelength_CKEf2, 3, "CKEf2-HandshakeLength0");
    memcpy(&((CKEf2 *)(record))->handshake_length, handshakelength_CKEf2, 3);
    break;
  case 3:
    klee_make_symbolic(&handshakelength_SHf1, 3, "SHf1-HandshakeLength0");
    memcpy(&((SHFRAG *)(record))->handshake_lengthf1, handshakelength_SHf1, 3);
    //
    klee_make_symbolic(&handshakelength_SHf2, 3, "SHf2-HandshakeLength0");
    memcpy(&((SHFRAG *)(record))->handshake_lengthf2, handshakelength_SHf2, 3);
    break;
  case 31:
    klee_make_symbolic(&handshakelength_SHf1, 3, "SHf1-HandshakeLength0");
    memcpy(&((SHf1 *)(record))->handshake_length, handshakelength_SHf1, 3);
    break;
  case 32:
    klee_make_symbolic(&handshakelength_SHf2, 3, "SHf2-HandshakeLength0");
    memcpy(&((SHf2 *)(record))->handshake_length, handshakelength_SHf2, 3);
    break;
  default:
    break;
  }
}

static void makeMessageSequenceSymbolicFrag(void *record, int rec_type)
{
  char messagesequence_CKEf1[2];
  char messagesequence_CKEf2[2];
  char messagesequence_SHf1[2];
  char messagesequence_SHf2[2];
  switch (rec_type)
  {
  case 5:
    klee_make_symbolic(&messagesequence_CKEf1, 2, "CKEf1-MessageSequence");
    memcpy(&((CKEFRAG *)(record))->message_sequencef1, messagesequence_CKEf1, 2);
    //
    klee_make_symbolic(&messagesequence_CKEf2, 2, "CKEf2-MessageSequence");
    memcpy(&((CKEFRAG *)(record))->message_sequencef2, messagesequence_CKEf2, 2);
    break;
  case 51:
    klee_make_symbolic(&messagesequence_CKEf1, 2, "CKEf1-MessageSequence");
    memcpy(&((CKEf1 *)(record))->message_sequence, messagesequence_CKEf1, 2);
    break;
  case 52:
    klee_make_symbolic(&messagesequence_CKEf2, 2, "CKEf2-MessageSequence");
    memcpy(&((CKEf2 *)(record))->message_sequence, messagesequence_CKEf2, 2);
    break;
  case 3:
    klee_make_symbolic(&messagesequence_SHf1, 2, "SHf1-MessageSequence");
    memcpy(&((SHFRAG *)(record))->message_sequencef1, messagesequence_SHf1, 2);
    //
    klee_make_symbolic(&messagesequence_SHf2, 2, "SHf2-MessageSequence");
    memcpy(&((SHFRAG *)(record))->message_sequencef2, messagesequence_SHf2, 2);
    break;
  case 31:
    klee_make_symbolic(&messagesequence_SHf1, 2, "SHf1-MessageSequence");
    memcpy(&((SHf1 *)(record))->message_sequence, messagesequence_SHf1, 2);
    break;
  case 32:
    klee_make_symbolic(&messagesequence_SHf2, 2, "SHf2-MessageSequence");
    memcpy(&((SHf2 *)(record))->message_sequence, messagesequence_SHf2, 2);
    break;
  default:
    break;
  }
}

static void makeHandshakeTypeSymbolic(void *record, int rec_type)
{
  char handshaketype_CH0;
  char handshaketype_CH2;
  char handshaketype_CKE;
  char handshaketype_HVR;
  char handshaketype_SH;
  char handshaketype_SHD;

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&handshaketype_CH0, 1, "CH0-handshaketype");
    ((CH0 *)(record))->handshake_type = handshaketype_CH0;
    break;
  case 2:
    klee_make_symbolic(&handshaketype_CH2, 1, "CH2-handshaketype");
    ((CH2 *)(record))->handshake_type = handshaketype_CH2;
    break;
  case 5:
    klee_make_symbolic(&handshaketype_CKE, 1, "CKE-handshaketype");
    ((CKE *)(record))->handshake_type = handshaketype_CKE;
    break;
  case 1:
    klee_make_symbolic(&handshaketype_HVR, 1, "HVR-handshaketype");
    ((HVR *)(record))->handshake_type = handshaketype_HVR;
    break;
  case 3:
    klee_make_symbolic(&handshaketype_SH, 1, "SH-handshaketype");
    ((SH *)(record))->handshake_type = handshaketype_SH;
    break;
  case 4:
    klee_make_symbolic(&handshaketype_SHD, 1, "SHD-handshaketype");
    ((SHD *)(record))->handshake_type = handshaketype_SHD;
    break;
  default:
    break;
  }
}

static void makeExtensionLengthSymbolic(void *record, int rec_type)
{
  char ExtensionLength_CH0[2];
  char ExtensionLength_CH2[2];
  char ExtensionLength_SH[2];

  switch (rec_type)
  {
  case 0:
    klee_make_symbolic(&ExtensionLength_CH0, 2, "CH0-ExtensionLength");
    ((CH0 *)record)->extension_length[0] = ExtensionLength_CH0[0];
    ((CH0 *)record)->extension_length[1] = ExtensionLength_CH0[1];
    break;
  case 2:
    klee_make_symbolic(&ExtensionLength_CH2, 2, "CH2-ExtensionLength");
    ((CH2 *)record)->extension_length[0] = ExtensionLength_CH2[0];
    ((CH2 *)record)->extension_length[1] = ExtensionLength_CH2[1];
    break;
  case 3:
    klee_make_symbolic(&ExtensionLength_SH, 2, "SH-ExtensionLength");
    ((SH *)record)->extension_length[0] = ExtensionLength_SH[0];
    ((SH *)record)->extension_length[1] = ExtensionLength_SH[1];
    break;
  default:
    break;
  }
}
/////////////////////////////

static bool isContentTypeValidServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  return (
      (client_hello1->content_type == 0x16) AND(client_hello2->content_type == 0x16) AND(client_key_exchange->content_type == 0x16) AND(client_change_cipher->content_type == 0x14));
}

static bool isContentTypeValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  return (
      (hello_verify_request->content_type == 0x16) AND(server_hello->content_type == 0x16) AND(server_hello_done->content_type == 0x16) AND(server_change_cipher->content_type == 0x14));
}

static bool isRecordVersionValidServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  return (
      ((((client_hello2->record_version[0]) == 0xfe) AND((client_hello2->record_version[1]) == 0xfd)) OR(((client_hello2->record_version[0]) == 0xfe) AND((client_hello2->record_version[1]) == 0xff))) AND((((client_key_exchange->record_version[0]) == 0xfe) AND((client_key_exchange->record_version[1]) == 0xfd)) OR(((client_key_exchange->record_version[0]) == 0xfe) AND((client_key_exchange->record_version[1]) == 0xff))) AND((((client_change_cipher->record_version[0]) == 0xfe) AND((client_change_cipher->record_version[1]) == 0xfd)) OR(((client_change_cipher->record_version[0]) == 0xfe) AND((client_change_cipher->record_version[1]) == 0xff))));
}

static bool isRecordVersionValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  return (
      ((((hello_verify_request->record_version[0]) == 0xfe) AND((hello_verify_request->record_version[1]) == 0xfd)) OR(((hello_verify_request->record_version[0]) == 0xfe) AND((hello_verify_request->record_version[1]) == 0xff))) AND((((server_hello->record_version[0]) == 0xfe) AND((server_hello->record_version[1]) == 0xfd)) OR(((server_hello->record_version[0]) == 0xfe) AND((server_hello->record_version[1]) == 0xff))) AND((((server_hello_done->record_version[0]) == 0xfe) AND((server_hello_done->record_version[1]) == 0xfd)) OR(((server_hello_done->record_version[0]) == 0xfe) AND((server_hello_done->record_version[1]) == 0xff))) AND((((server_change_cipher->record_version[0]) == 0xfe) AND((server_change_cipher->record_version[1]) == 0xfd)) OR(((server_change_cipher->record_version[0]) == 0xfe) AND((server_change_cipher->record_version[1]) == 0xff))));
}

static bool isEpochValidServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  return (
      ((client_hello1->epoch[0] == 0x00) AND(client_hello1->epoch[1] == 0x00)) AND((client_hello2->epoch[0] == 0x00) AND(client_hello2->epoch[1] == 0x00)) AND((client_key_exchange->epoch[0] == 0x00) AND(client_key_exchange->epoch[1] == 0x00)) AND((client_change_cipher->epoch[0] == 0x00) AND(client_change_cipher->epoch[1] == 0x00)));
}

static bool isEpochValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  return (
      ((hello_verify_request->epoch[0] == 0x00) AND(hello_verify_request->epoch[1] == 0x00)) AND((server_hello->epoch[0] == 0x00) AND(server_hello->epoch[1] == 0x00)) AND((server_hello_done->epoch[0] == 0x00) AND(server_hello_done->epoch[1] == 0x00)) AND((server_change_cipher->epoch[0] == 0x00) AND(server_change_cipher->epoch[1] == 0x00)));
}

static bool isRecordSequenceUniqueServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  return (
      (client_hello2->sequence_number[5] != client_key_exchange->sequence_number[5]) AND(client_hello2->sequence_number[5] != client_change_cipher->sequence_number[5]) AND(client_key_exchange->sequence_number[5] != client_change_cipher->sequence_number[5]));
}

static bool isRecordSequenceUniqueClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  return (
      (hello_verify_request->sequence_number[5] != server_hello->sequence_number[5]) AND(hello_verify_request->sequence_number[5] != server_hello_done->sequence_number[5]) AND(hello_verify_request->sequence_number[5] != server_change_cipher->sequence_number[5]) AND(server_hello->sequence_number[5] != server_hello_done->sequence_number[5]) AND(server_hello->sequence_number[5] != server_change_cipher->sequence_number[5]) AND(server_hello_done->sequence_number[5] != server_change_cipher->sequence_number[5]));
}

static bool isInWindowOrAfter(uint8_t a, uint8_t b, uint8_t win_size)
{
  return (a >= b) | ((a < b) & ((b < win_size) | ((b >= win_size) & (a >= b - win_size))));
}

static void maxSymbolic(uint8_t output, uint8_t a, uint8_t b)
{
  klee_assume(
      ((a >= b) AND(output == a)) OR((a < b) AND(output == b)));
}

static bool isRecordSequenceValidServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  uint8_t rightEdge0, rightEdge1, rightEdge2;

  klee_make_symbolic(&rightEdge1, sizeof(rightEdge1), "RE1");
  klee_make_symbolic(&rightEdge2, sizeof(rightEdge2), "RE2");
  rightEdge0 = client_hello2->sequence_number[5];

  maxSymbolic(rightEdge1, client_key_exchange->sequence_number[5], rightEdge0);
  maxSymbolic(rightEdge2, client_change_cipher->sequence_number[5], rightEdge1);

  return (
      isInWindowOrAfter(client_key_exchange->sequence_number[5], rightEdge0, 0x40) AND
          isInWindowOrAfter(client_change_cipher->sequence_number[5], rightEdge1, 0x40));
}

static bool isRecordSequenceValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  uint8_t rightEdge0, rightEdge1, rightEdge2;

  klee_make_symbolic(&rightEdge1, sizeof(rightEdge1), "RE1");
  klee_make_symbolic(&rightEdge2, sizeof(rightEdge2), "RE2");

  rightEdge0 = server_hello->sequence_number[5];

  maxSymbolic(rightEdge1, server_hello_done->sequence_number[5], rightEdge0);
  maxSymbolic(rightEdge2, server_change_cipher->sequence_number[5], rightEdge1);

  return (
      isInWindowOrAfter(server_hello->sequence_number[5], hello_verify_request->sequence_number[5], 0x40) AND
          isInWindowOrAfter(server_hello_done->sequence_number[5], rightEdge0, 0x40) AND
              isInWindowOrAfter(server_change_cipher->sequence_number[5], rightEdge1, 0x40));
}

static bool isRecordLenValidServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                   CCS *client_change_cipher, CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange,
                                   CCS *shadow_client_change_cipher)
{
  return (
      ((client_hello1->record_length[0] == shadow_client_hello1->record_length[0]) AND(client_hello1->record_length[1] == shadow_client_hello1->record_length[1]))
          AND((client_hello2->record_length[0] == shadow_client_hello2->record_length[0]) AND(client_hello2->record_length[1] == shadow_client_hello2->record_length[1]))
              AND((client_key_exchange->record_length[0] == shadow_client_key_exchange->record_length[0]) AND(client_key_exchange->record_length[1] == shadow_client_key_exchange->record_length[1]))
                  AND((client_change_cipher->record_length[0] == shadow_client_change_cipher->record_length[0]) AND(client_change_cipher->record_length[1] == shadow_client_change_cipher->record_length[1])));
}

static bool isRecordLenValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                   CCS *server_change_cipher, HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done,
                                   CCS *shadow_server_change_cipher)
{
  return (
      ((hello_verify_request->record_length[0] == shadow_hello_verify_request->record_length[0]) AND(hello_verify_request->record_length[1] == shadow_hello_verify_request->record_length[1]))
          AND((server_hello->record_length[0] == shadow_server_hello->record_length[0]) AND(server_hello->record_length[1] == shadow_server_hello->record_length[1]))
              AND((server_hello_done->record_length[0] == shadow_server_hello_done->record_length[0]) AND(server_hello_done->record_length[1] == shadow_server_hello_done->record_length[1]))
                  AND((server_change_cipher->record_length[0] == shadow_server_change_cipher->record_length[0]) AND(server_change_cipher->record_length[1] == shadow_server_change_cipher->record_length[1])));
}

////////////
// Handshake type

static bool isHandshakeTypeValidServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange)
{
  return(
    (client_hello1->handshake_type == shadow_client_hello1->handshake_type) AND
    (client_hello2->handshake_type == shadow_client_hello2->handshake_type) AND
    (client_key_exchange->handshake_type == shadow_client_key_exchange->handshake_type)
  );
}

static bool isHandshakeTypeValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done)
{
  return(
    (hello_verify_request->handshake_type == shadow_hello_verify_request->handshake_type) AND
    (server_hello->handshake_type == shadow_server_hello->handshake_type) AND
    (server_hello_done->handshake_type == shadow_server_hello_done->handshake_type)
  );
}

static bool isMessageLenValidServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                    CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange)
{
  return (
      ((client_hello1->handshake_length[0] == shadow_client_hello1->handshake_length[0]) AND(client_hello1->handshake_length[1] == shadow_client_hello1->handshake_length[1]) AND(client_hello1->handshake_length[2] == shadow_client_hello1->handshake_length[2]))
          AND((client_hello2->handshake_length[0] == shadow_client_hello2->handshake_length[0]) AND(client_hello2->handshake_length[1] == shadow_client_hello2->handshake_length[1]) AND(client_hello2->handshake_length[2] == shadow_client_hello2->handshake_length[2]))
              AND((client_key_exchange->handshake_length[0] == shadow_client_key_exchange->handshake_length[0]) AND(client_key_exchange->handshake_length[1] == shadow_client_key_exchange->handshake_length[1]) AND(client_key_exchange->handshake_length[2] == shadow_client_key_exchange->handshake_length[2])));
}

static bool isMessageLenValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                    HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done)
{
  return (
      ((hello_verify_request->handshake_length[0] == shadow_hello_verify_request->handshake_length[0]) AND(hello_verify_request->handshake_length[1] == shadow_hello_verify_request->handshake_length[1]) AND(hello_verify_request->handshake_length[2] == shadow_hello_verify_request->handshake_length[2]))
          AND((server_hello->handshake_length[0] == shadow_server_hello->handshake_length[0]) AND(server_hello->handshake_length[1] == shadow_server_hello->handshake_length[1]) AND(server_hello->handshake_length[2] == shadow_server_hello->handshake_length[2]))
              AND((server_hello_done->handshake_length[0] == shadow_server_hello_done->handshake_length[0]) AND(server_hello_done->handshake_length[1] == shadow_server_hello_done->handshake_length[1]) AND(server_hello_done->handshake_length[2] == shadow_server_hello_done->handshake_length[2])));
}

static bool isMessageSequenceValidServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  return (
      (client_hello1->message_sequence[1] == 0x00) AND(client_hello2->message_sequence[1] == 0x01) AND(client_key_exchange->message_sequence[1] == 0x02));
}

static bool isMessageSequenceValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  return (
      (hello_verify_request->message_sequence[1] == 0x00) AND(server_hello->message_sequence[1] == 0x01) AND(server_hello_done->message_sequence[1] == 0x02));
}

static bool isFragmentLenValidServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                     CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange)
{
  return (
      ((client_hello1->fragment_length[0] == shadow_client_hello1->fragment_length[0]) AND(client_hello1->fragment_length[1] == shadow_client_hello1->fragment_length[1]) AND(client_hello1->fragment_length[2] == shadow_client_hello1->fragment_length[2]))
          AND((client_hello2->fragment_length[0] == shadow_client_hello2->fragment_length[0]) AND(client_hello2->fragment_length[1] == shadow_client_hello2->fragment_length[1]) AND(client_hello2->fragment_length[2] == shadow_client_hello2->fragment_length[2]))
              AND((client_key_exchange->fragment_length[0] == shadow_client_key_exchange->fragment_length[0]) AND(client_key_exchange->fragment_length[1] == shadow_client_key_exchange->fragment_length[1]) AND(client_key_exchange->fragment_length[2] == shadow_client_key_exchange->fragment_length[2])));
}

static bool isFragmentLenValidClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                     HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done)
{
  return (
      ((hello_verify_request->fragment_length[0] == shadow_hello_verify_request->fragment_length[0]) AND(hello_verify_request->fragment_length[1] == shadow_hello_verify_request->fragment_length[1]) AND(hello_verify_request->fragment_length[2] == shadow_hello_verify_request->fragment_length[2]))
          AND((server_hello->fragment_length[0] == shadow_server_hello->fragment_length[0]) AND(server_hello->fragment_length[1] == shadow_server_hello->fragment_length[1]) AND(server_hello->fragment_length[2] == shadow_server_hello->fragment_length[2]))
              AND((server_hello_done->fragment_length[0] == shadow_server_hello_done->fragment_length[0]) AND(server_hello_done->fragment_length[1] == shadow_server_hello_done->fragment_length[1]) AND(server_hello_done->fragment_length[2] == shadow_server_hello_done->fragment_length[2])));
}

static bool isOffsetValidNoFragServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  return (
      (client_hello1->fragment_offset[2] == 0x00) AND(client_hello2->fragment_offset[2] == 0x00) AND(client_key_exchange->fragment_offset[2] == 0x00));
}

static bool isOffsetValidNoFragClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  return (
      (hello_verify_request->fragment_offset[2] == 0x00) AND(server_hello->fragment_offset[2] == 0x00) AND(server_hello_done->fragment_offset[2] == 0x00));
}

static bool isFragmentLenMessageLenEqualServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  return (
      (client_hello1->fragment_length[0] == client_hello1->handshake_length[0]) AND(client_hello1->fragment_length[1] == client_hello1->handshake_length[1]) AND(client_hello1->fragment_length[2] == client_hello1->handshake_length[2]) AND(client_hello2->fragment_length[0] == client_hello2->handshake_length[0]) AND(client_hello2->fragment_length[1] == client_hello2->handshake_length[1]) AND(client_hello2->fragment_length[2] == client_hello2->handshake_length[2]) AND(client_key_exchange->fragment_length[0] == client_key_exchange->handshake_length[0]) AND(client_key_exchange->fragment_length[1] == client_key_exchange->handshake_length[1]) AND(client_key_exchange->fragment_length[2] == client_key_exchange->handshake_length[2]));
}

static bool isFragmentLenMessageLenEqualClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  return (
      ((hello_verify_request->fragment_length[0] == hello_verify_request->handshake_length[0]) AND(hello_verify_request->fragment_length[1] == hello_verify_request->handshake_length[1]) AND(hello_verify_request->fragment_length[2] == hello_verify_request->handshake_length[2])) AND((server_hello->fragment_length[0] == server_hello->handshake_length[0]) AND(server_hello->fragment_length[1] == server_hello->handshake_length[1]) AND(server_hello->fragment_length[2] == server_hello->handshake_length[2])) AND((server_hello_done->fragment_length[0] == server_hello_done->handshake_length[0]) AND(server_hello_done->fragment_length[1] == server_hello_done->handshake_length[1]) AND(server_hello_done->fragment_length[2] == server_hello_done->handshake_length[2])));
}

static bool isByteContainedFragServer(CKEFRAG *client_key_exchange, unsigned int b)
{

  return (
    (((unsigned int)(client_key_exchange->fragment_offsetf1[2]) <= b) AND
    (b < (unsigned int)(client_key_exchange->fragment_offsetf1[2]) + (unsigned int)(client_key_exchange->fragment_lengthf1[2]))) OR
    (((unsigned int)(client_key_exchange->fragment_offsetf2[2]) <= b) AND
    (b < (unsigned int)(client_key_exchange->fragment_offsetf2[2]) + (unsigned int)(client_key_exchange->fragment_lengthf2[2])))
  );
}

static bool isByteContainedFragServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2, unsigned int b)
{
  return (
    (((unsigned int)(client_key_exchangef1->fragment_offset[2]) <= b) AND
    (b < (unsigned int)(client_key_exchangef1->fragment_offset[2]) + (unsigned int)(client_key_exchangef1->fragment_length[2]))) OR
    (((unsigned int)(client_key_exchangef2->fragment_offset[2]) <= b) AND
    (b < (unsigned int)(client_key_exchangef2->fragment_offset[2]) + (unsigned int)(client_key_exchangef2->fragment_length[2])))
  );
}

static bool isByteContainedFragClient(SHFRAG *server_hello, unsigned int b)
{
  return (
    (((unsigned int)(server_hello->fragment_offsetf1[2]) <= b) AND
    (b < (unsigned int)(server_hello->fragment_offsetf1[2]) + (unsigned int)(server_hello->fragment_lengthf1[2]))) OR
    (((unsigned int)(server_hello->fragment_offsetf2[2]) <= b) AND
    (b < (unsigned int)(server_hello->fragment_offsetf2[2]) + (unsigned int)(server_hello->fragment_lengthf2[2])))
  );
}

static bool isByteContainedFragClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2, unsigned int b)
{
  
  return (
    (((unsigned int)(server_hellof1->fragment_offset[2]) <= b) AND
    (b < (unsigned int)(server_hellof1->fragment_offset[2]) + (unsigned int)(server_hellof1->fragment_length[2]))) OR
    (((unsigned int)(server_hellof2->fragment_offset[2]) <= b) AND
    (b < (unsigned int)(server_hellof2->fragment_offset[2]) + (unsigned int)(server_hellof2->fragment_length[2])))
  );

}

static bool isMessageSequenceEqualFragServer(CKEFRAG *client_key_exchange)
{
  klee_assume(client_key_exchange->message_sequencef1[0] == 0x00);
  klee_assume(client_key_exchange->message_sequencef2[0] == 0x00);

  return (
      (client_key_exchange->message_sequencef1[1] == client_key_exchange->message_sequencef2[1]));
}

static bool isMessageSequenceEqualFragServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2)
{
  klee_assume(client_key_exchangef1->message_sequence[0] == 0x00);
  klee_assume(client_key_exchangef2->message_sequence[0] == 0x00);

  return (
      (client_key_exchangef1->message_sequence[1] == client_key_exchangef2->message_sequence[1]));
}

static bool isMessageSequenceEqualFragClient(SHFRAG *server_hello)
{
  klee_assume(server_hello->message_sequencef1[0] == 0x00);
  klee_assume(server_hello->message_sequencef2[0] == 0x00);

  return (
      (server_hello->message_sequencef1[1] == server_hello->message_sequencef2[1]));
}

static bool isMessageSequenceEqualFragClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2)
{
  klee_assume(server_hellof1->message_sequence[0] == 0x00);
  klee_assume(server_hellof2->message_sequence[0] == 0x00);

  return (
      (server_hellof1->message_sequence[1] == server_hellof2->message_sequence[1]));
}

static bool isMessageLenEqualFragServer(CKEFRAG *client_key_exchange)
{
  klee_assume(client_key_exchange->handshake_lengthf1[0] == 0x00);
  klee_assume(client_key_exchange->handshake_lengthf1[1] == 0x00);

  klee_assume(client_key_exchange->handshake_lengthf2[0] == 0x00);
  klee_assume(client_key_exchange->handshake_lengthf2[1] == 0x00);

  return (
      (client_key_exchange->handshake_lengthf1[2] == client_key_exchange->handshake_lengthf2[2]));
}

static bool isMessageLenEqualFragServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2)
{
  klee_assume(client_key_exchangef1->handshake_length[0] == 0x00);
  klee_assume(client_key_exchangef1->handshake_length[1] == 0x00);

  klee_assume(client_key_exchangef2->handshake_length[0] == 0x00);
  klee_assume(client_key_exchangef2->handshake_length[1] == 0x00);

  return (
      (client_key_exchangef1->handshake_length[2] == client_key_exchangef2->handshake_length[2]));
}

static bool isMessageLenEqualFragClient(SHFRAG *server_hello)
{
  klee_assume(server_hello->handshake_lengthf1[0] == 0x00);
  klee_assume(server_hello->handshake_lengthf1[1] == 0x00);

  klee_assume(server_hello->handshake_lengthf2[0] == 0x00);
  klee_assume(server_hello->handshake_lengthf2[1] == 0x00);

  return (
      (server_hello->handshake_lengthf1[2] == server_hello->handshake_lengthf2[2]));
}

static bool isMessageLenEqualFragClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2)
{
  klee_assume(server_hellof1->handshake_length[0] == 0x00);
  klee_assume(server_hellof1->handshake_length[1] == 0x00);

  klee_assume(server_hellof2->handshake_length[0] == 0x00);
  klee_assume(server_hellof2->handshake_length[1] == 0x00);

  return (
      (server_hellof1->handshake_length[2] == server_hellof2->handshake_length[2]));
}


/*
static bool isFragmentLenRecordLenRelatedServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  klee_assume(
      (client_hello1->fragment_length[0] == 0x00)
          AND(client_hello2->fragment_length[0] == 0x00)
              AND(client_key_exchange->fragment_length[0] == 0x00));
  return (
      (((client_hello1->record_length[0] - client_hello1->fragment_length[1]) == 0x00) AND((client_hello1->record_length[1] - client_hello1->fragment_length[2]) == 0x0c)) AND(((client_hello2->record_length[0] - client_hello2->fragment_length[1]) == 0x00) AND((client_hello2->record_length[1] - client_hello2->fragment_length[2]) == 0x0c)) AND(((client_key_exchange->record_length[0] - client_key_exchange->fragment_length[1]) == 0x00) AND((client_key_exchange->record_length[1] - client_key_exchange->fragment_length[2]) == 0x0c)));
}

static bool isFragmentLenRecordLenRelatedClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  klee_assume(
      (hello_verify_request->fragment_length[0] == 0x00)
          AND(server_hello->fragment_length[0] == 0x00)
              AND(server_hello_done->fragment_length[0] == 0x00));
  return (
      (((hello_verify_request->record_length[0] - hello_verify_request->fragment_length[1]) == 0x00) AND((hello_verify_request->record_length[1] - hello_verify_request->fragment_length[2]) == 0x0c)) AND(((server_hello->record_length[0] - server_hello->fragment_length[1]) == 0x00) AND((server_hello->record_length[1] - server_hello->fragment_length[2]) == 0x0c)) AND(((server_hello_done->record_length[0] - server_hello_done->fragment_length[1]) == 0x00) AND((server_hello_done->record_length[1] - server_hello_done->fragment_length[2]) == 0x0c)));
}

static bool isMessageLenRecordLenRelatedServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  klee_assume(
      (client_hello1->handshake_length[0] == 0x00)
          AND(client_hello2->handshake_length[0] == 0x00)
              AND(client_key_exchange->handshake_length[0] == 0x00));
  return (
      (((client_hello1->record_length[0] - client_hello1->handshake_length[1]) == 0x00) AND((client_hello1->record_length[1] - client_hello1->handshake_length[2]) == 0x0c)) AND(((client_hello2->record_length[0] - client_hello2->handshake_length[1]) == 0x00) AND((client_hello2->record_length[1] - client_hello2->handshake_length[2]) == 0x0c)) AND(((client_key_exchange->record_length[0] - client_key_exchange->handshake_length[1]) == 0x00) AND((client_key_exchange->record_length[1] - client_key_exchange->handshake_length[2]) == 0x0c)));
}

static bool isMessageLenRecordLenRelatedClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  klee_assume(
      (hello_verify_request->handshake_length[0] == 0x00)
          AND(server_hello->handshake_length[0] == 0x00)
              AND(server_hello_done->handshake_length[0] == 0x00));
  return (
      (((hello_verify_request->record_length[0] - hello_verify_request->handshake_length[1]) == 0x00) AND((hello_verify_request->record_length[1] - hello_verify_request->handshake_length[2]) == 0x0c)) AND(((server_hello->record_length[0] - server_hello->handshake_length[1]) == 0x00) AND((server_hello->record_length[1] - server_hello->handshake_length[2]) == 0x0c)) AND(((server_hello_done->record_length[0] - server_hello_done->handshake_length[1]) == 0x00) AND((server_hello_done->record_length[1] - server_hello_done->handshake_length[2]) == 0x0c)));
}
*/

static bool isHandshakeVersionValidServer(CH0 *client_hello1, CH2 *client_hello2)
{
  return (
      ((((client_hello1->handshake_version[0]) == 0xfe) AND((client_hello1->handshake_version[1]) == 0xfd)) OR(((client_hello1->handshake_version[0]) == 0xfe) AND((client_hello1->handshake_version[1]) == 0xff))) AND((((client_hello2->handshake_version[0]) == 0xfe) AND((client_hello2->handshake_version[1]) == 0xfd)) OR(((client_hello2->handshake_version[0]) == 0xfe) AND((client_hello2->handshake_version[1]) == 0xff))));
}

static bool isHandshakeVersionValidClient(HVR *hello_verify_request, SH *server_hello)
{
  return (
      ((((hello_verify_request->handshake_version[0]) == 0xfe) AND((hello_verify_request->handshake_version[1]) == 0xfd)) OR(((hello_verify_request->handshake_version[0]) == 0xfe) AND((hello_verify_request->handshake_version[1]) == 0xff))) AND((((server_hello->handshake_version[0]) == 0xfe) AND((server_hello->handshake_version[1]) == 0xfd)) OR(((server_hello->handshake_version[0]) == 0xfe) AND((server_hello->handshake_version[1]) == 0xff))));
}

static bool isCookieLengthValidServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                                      CH2 *shadow_client_hello2)
{
  return (
      (client_hello1->cookie_length == shadow_client_hello1->cookie_length) AND(client_hello2->cookie_length == shadow_client_hello2->cookie_length));
}

static bool isCookieLengthValidClient(HVR *hello_verify_request, HVR *shadow_hello_verify_request)
{
  return (
      (hello_verify_request->cookie_length == shadow_hello_verify_request->cookie_length));
}

static bool isSessionIDLengthValidServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                                         CH2 *shadow_client_hello2)
{
  return (
      (client_hello1->session_id_length == shadow_client_hello1->session_id_length) AND(client_hello2->session_id_length == shadow_client_hello2->session_id_length));
}

static bool isSessionIDLengthValidClient(SH *server_hello, SH *shadow_server_hello)
{
  return (
      (server_hello->session_id_length == shadow_server_hello->session_id_length));
}

static bool isExtensionLengthValidServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                           CH2 *shadow_client_hello2)
{
  return(
    (client_hello1->extension_length[0] == shadow_client_hello1->extension_length[0]) AND 
    (client_hello1->extension_length[1] == shadow_client_hello1->extension_length[1]) AND
    (client_hello2->extension_length[0] == shadow_client_hello2->extension_length[0]) AND 
    (client_hello2->extension_length[1] == shadow_client_hello2->extension_length[1])
  );
}

static bool isExtensionLengthValidClient(SH *server_hello, SH *shadow_server_hello)
{
  return(
    (server_hello->extension_length[0] == shadow_server_hello->extension_length[0]) AND
    (server_hello->extension_length[1] == shadow_server_hello->extension_length[1])
  );
}

/*

static bool isOffsetValidFragServer(CKEFRAG *client_key_exchange, CKEFRAG *shadow_client_key_exchange)
{
  return (
      (client_key_exchange->fragment_offsetf1[2] == shadow_client_key_exchange->fragment_offsetf1[2]) AND(client_key_exchange->fragment_offsetf2[2] == shadow_client_key_exchange->fragment_offsetf2[2]));
}

static bool isOffsetValidFragServerTiny(CKEf1 *client_key_exchangef1, CKEf1 *shadow_client_key_exchangef1,
                                        CKEf2 *client_key_exchangef2, CKEf2 *shadow_client_key_exchangef2)
{
  return (
      (client_key_exchangef1->fragment_offset[2] == shadow_client_key_exchangef1->fragment_offset[2]) AND(client_key_exchangef2->fragment_offset[2] == shadow_client_key_exchangef2->fragment_offset[2]));
}

static bool isOffsetValidFragClient(SHFRAG *server_hello, SHFRAG *shadow_server_hello)
{
  return (
      (server_hello->fragment_offsetf1[2] == shadow_server_hello->fragment_offsetf1[2]) AND(server_hello->fragment_offsetf2[2] == shadow_server_hello->fragment_offsetf2[2]));
}

static bool isOffsetValidFragClientTiny(SHf1 *server_hellof1, SHf1 *shadow_server_hellof1,
                                        SHf2 *server_hellof2, SHf2 *shadow_server_hellof2)
{
  return (
      (server_hellof1->fragment_offset[2] == shadow_server_hellof1->fragment_offset[2]) AND(server_hellof2->fragment_offset[2] == shadow_server_hellof2->fragment_offset[2]));
}

static bool isFragmentLenMessageLenRelatedServer(CKEFRAG *client_key_exchange)
{
  klee_assume(client_key_exchange->handshake_lengthf1[0] == 0x00);
  klee_assume(client_key_exchange->handshake_lengthf1[1] == 0x00);

  klee_assume(client_key_exchange->handshake_lengthf2[0] == 0x00);
  klee_assume(client_key_exchange->handshake_lengthf2[1] == 0x00);

  klee_assume(client_key_exchange->fragment_lengthf1[0] == 0x00);
  klee_assume(client_key_exchange->fragment_lengthf1[1] == 0x00);

  klee_assume(client_key_exchange->fragment_lengthf2[0] == 0x00);
  klee_assume(client_key_exchange->fragment_lengthf2[1] == 0x00);

  return (
      (client_key_exchange->handshake_lengthf1[2] <=
       client_key_exchange->fragment_lengthf1[2] + client_key_exchange->fragment_lengthf2[2]) AND(client_key_exchange->handshake_lengthf2[2] <=
                                                                                                  client_key_exchange->fragment_lengthf1[2] + client_key_exchange->fragment_lengthf2[2]));
}

static bool isFragmentLenMessageLenRelatedServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2)
{
  klee_assume(client_key_exchangef1->handshake_length[0] == 0x00);
  klee_assume(client_key_exchangef1->handshake_length[1] == 0x00);

  klee_assume(client_key_exchangef2->handshake_length[0] == 0x00);
  klee_assume(client_key_exchangef2->handshake_length[1] == 0x00);

  klee_assume(client_key_exchangef1->fragment_length[0] == 0x00);
  klee_assume(client_key_exchangef1->fragment_length[1] == 0x00);

  klee_assume(client_key_exchangef2->fragment_length[0] == 0x00);
  klee_assume(client_key_exchangef2->fragment_length[1] == 0x00);

  return (
      (client_key_exchangef1->handshake_length[2] <=
       client_key_exchangef1->fragment_length[2] + client_key_exchangef2->fragment_length[2]) AND(client_key_exchangef2->handshake_length[2] <=
                                                                                                  client_key_exchangef1->fragment_length[2] + client_key_exchangef2->fragment_length[2]));
}

static bool isFragmentLenMessageLenRelatedClient(SHFRAG *server_hello)
{
  klee_assume(server_hello->handshake_lengthf1[0] == 0x00);
  klee_assume(server_hello->handshake_lengthf1[1] == 0x00);

  klee_assume(server_hello->handshake_lengthf2[0] == 0x00);
  klee_assume(server_hello->handshake_lengthf2[1] == 0x00);

  klee_assume(server_hello->fragment_lengthf1[0] == 0x00);
  klee_assume(server_hello->fragment_lengthf1[1] == 0x00);

  klee_assume(server_hello->fragment_lengthf2[0] == 0x00);
  klee_assume(server_hello->fragment_lengthf2[1] == 0x00);

  return (
      (server_hello->handshake_lengthf1[2] <=
       server_hello->fragment_lengthf1[2] + server_hello->fragment_lengthf2[2]) AND(server_hello->handshake_lengthf2[2] <=
                                                                                    server_hello->fragment_lengthf1[2] + server_hello->fragment_lengthf2[2]));
}

static bool isFragmentLenMessageLenRelatedClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2)
{
  klee_assume(server_hellof1->handshake_length[0] == 0x00);
  klee_assume(server_hellof1->handshake_length[1] == 0x00);

  klee_assume(server_hellof2->handshake_length[0] == 0x00);
  klee_assume(server_hellof2->handshake_length[1] == 0x00);

  klee_assume(server_hellof1->fragment_length[0] == 0x00);
  klee_assume(server_hellof1->fragment_length[1] == 0x00);

  klee_assume(server_hellof2->fragment_length[0] == 0x00);
  klee_assume(server_hellof2->fragment_length[1] == 0x00);

  return (
      (server_hellof1->handshake_length[2] <=
       server_hellof1->fragment_length[2] + server_hellof2->fragment_length[2]) AND(server_hellof2->handshake_length[2] <=
                                                                                    server_hellof1->fragment_length[2] + server_hellof2->fragment_length[2]));
}
*/



////////////////////////////
void contentTypeServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  makeContentTypeSybmolic(client_hello1, 0);
  makeContentTypeSybmolic(client_hello2, 2);
  makeContentTypeSybmolic(client_key_exchange, 5);
  makeContentTypeSybmolic(client_change_cipher, 6);

  klee_assume(
      NEGATION(isContentTypeValidServer(client_hello1, client_hello2, client_key_exchange, client_change_cipher)));
}

void contentTypeClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  makeContentTypeSybmolic(hello_verify_request, 1);
  makeContentTypeSybmolic(server_hello, 3);
  makeContentTypeSybmolic(server_hello_done, 4);
  makeContentTypeSybmolic(server_change_cipher, 8);

  klee_assume(
      NEGATION(isContentTypeValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher)));
}

void recordVersionServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  makeRecordVersionSymbolic(client_hello2, 2);
  makeRecordVersionSymbolic(client_key_exchange, 5);
  makeRecordVersionSymbolic(client_change_cipher, 6);

  klee_assume(
      NEGATION(isRecordVersionValidServer(client_hello2, client_key_exchange, client_change_cipher)));
}

void recordVersionClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  makeRecordVersionSymbolic(hello_verify_request, 1);
  makeRecordVersionSymbolic(server_hello, 3);
  makeRecordVersionSymbolic(server_hello_done, 4);
  makeRecordVersionSymbolic(server_change_cipher, 8);

  klee_assume(
      NEGATION(isRecordVersionValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher)));
}

void epochServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  makeEpochSymbolic(client_hello1, 0);
  makeEpochSymbolic(client_hello2, 2);
  makeEpochSymbolic(client_key_exchange, 5);
  makeEpochSymbolic(client_change_cipher, 6);

  klee_assume(
      NEGATION(
          isEpochValidServer(client_hello1, client_hello2, client_key_exchange, client_change_cipher)));
}

void epochClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  makeEpochSymbolic(hello_verify_request, 1);
  makeEpochSymbolic(server_hello, 3);
  makeEpochSymbolic(server_hello_done, 4);
  makeEpochSymbolic(server_change_cipher, 8);

  klee_assume(
      NEGATION(
          isEpochValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher)));
}

void sequenceUniquenessServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  ////
  makeSequenceSymbolic(client_hello2, 2);
  makeSequenceSymbolic(client_key_exchange, 5);
  makeSequenceSymbolic(client_change_cipher, 6);

  klee_assume(
      NEGATION(isRecordSequenceUniqueServer(client_hello2, client_key_exchange, client_change_cipher)));
}

void sequenceUniquenessClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{
  makeSequenceSymbolic(hello_verify_request, 1);
  makeSequenceSymbolic(server_hello, 3);
  makeSequenceSymbolic(server_hello_done, 4);
  makeSequenceSymbolic(server_change_cipher, 8);

  klee_assume(
      NEGATION(isRecordSequenceUniqueClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher)));
}

void sequenceWindowServer(CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher)
{
  makeSequenceSymbolic(client_hello2, 2);
  makeSequenceSymbolic(client_key_exchange, 5);
  makeSequenceSymbolic(client_change_cipher, 6);

  klee_assume(
      NEGATION(isRecordSequenceValidServer(client_hello2, client_key_exchange, client_change_cipher)));
}

void sequenceWindowClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher)
{

  makeSequenceSymbolic(hello_verify_request, 1);
  makeSequenceSymbolic(server_hello, 3);
  makeSequenceSymbolic(server_hello_done, 4);
  makeSequenceSymbolic(server_change_cipher, 8);

  klee_assume(
      NEGATION(isRecordSequenceValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher)));
}

void recordLengthValidityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                CCS *client_change_cipher, CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange,
                                CCS *shadow_client_change_cipher)
{
  makeRecordLenSymbolic(client_hello1, 0);
  makeRecordLenSymbolic(client_hello2, 2);
  makeRecordLenSymbolic(client_key_exchange, 5);
  makeRecordLenSymbolic(client_change_cipher, 6);

  klee_assume(
      NEGATION(
          isRecordLenValidServer(client_hello1, client_hello2, client_key_exchange, client_change_cipher,
                                 shadow_client_hello1, shadow_client_hello2, shadow_client_key_exchange, shadow_client_change_cipher)));
}

void recordLengthValidityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                CCS *server_change_cipher, HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done,
                                CCS *shadow_server_change_cipher)
{
  makeRecordLenSymbolic(hello_verify_request, 1);
  makeRecordLenSymbolic(server_hello, 3);
  makeRecordLenSymbolic(server_hello_done, 4);
  makeRecordLenSymbolic(server_change_cipher, 8);

  klee_assume(
      NEGATION(
          isRecordLenValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher,
                                 shadow_hello_verify_request, shadow_server_hello, shadow_server_hello_done, shadow_server_change_cipher)));
}

void handshakeTypeValidityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                 CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange)
{
  makeHandshakeTypeSymbolic(client_hello1, 0);
  makeHandshakeTypeSymbolic(client_hello2, 2);
  makeHandshakeTypeSymbolic(client_key_exchange, 5);

  klee_assume(
    NEGATION(isHandshakeTypeValidServer(client_hello1, client_hello2, client_key_exchange, shadow_client_hello1, 
    shadow_client_hello2, shadow_client_key_exchange))
  );
}

void handshakeTypeValidityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                 HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done)
{
  makeHandshakeTypeSymbolic(hello_verify_request, 1);
  makeHandshakeTypeSymbolic(server_hello, 3);
  makeHandshakeTypeSymbolic(server_hello_done, 4);

  klee_assume(
    NEGATION(isHandshakeTypeValidClient(hello_verify_request, server_hello, server_hello_done, shadow_hello_verify_request, 
    shadow_server_hello, shadow_server_hello_done))
  );
}

void messageLengthValidityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                 CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange)
{
  makeMessageLenSymbolicNoFrag(client_hello1, 0);
  makeMessageLenSymbolicNoFrag(client_hello2, 2);
  makeMessageLenSymbolicNoFrag(client_key_exchange, 5);

  klee_assume(
      NEGATION(isMessageLenValidServer(client_hello1, client_hello2, client_key_exchange, shadow_client_hello1,
                                       shadow_client_hello2, shadow_client_key_exchange)));
}

void messageLengthValidityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                 HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done)
{
  makeMessageLenSymbolicNoFrag(hello_verify_request, 1);
  makeMessageLenSymbolicNoFrag(server_hello, 3);
  makeMessageLenSymbolicNoFrag(server_hello_done, 4);

  klee_assume(
      NEGATION(isMessageLenValidClient(hello_verify_request, server_hello, server_hello_done, shadow_hello_verify_request,
                                       shadow_server_hello, shadow_server_hello_done)));
}

void messageSequenceServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  makeMessageSequenceSymbolic(client_hello1, 0);
  makeMessageSequenceSymbolic(client_hello2, 2);
  makeMessageSequenceSymbolic(client_key_exchange, 5);

  klee_assume(
      NEGATION(isMessageSequenceValidServer(client_hello1, client_hello2, client_key_exchange)));
}

void messageSequenceClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  makeMessageSequenceSymbolic(hello_verify_request, 1);
  makeMessageSequenceSymbolic(server_hello, 3);
  makeMessageSequenceSymbolic(server_hello_done, 4);

  klee_assume(
      NEGATION(isMessageSequenceValidClient(hello_verify_request, server_hello, server_hello_done)));
}

void fragmentLengthValidityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange,
                                  CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange)
{
  makeFragmentLenSymbolicNoFrag(client_hello1, 0);
  makeFragmentLenSymbolicNoFrag(client_hello2, 2);
  makeFragmentLenSymbolicNoFrag(client_key_exchange, 5);

  klee_assume(
      NEGATION(isFragmentLenValidServer(client_hello1, client_hello2, client_key_exchange, shadow_client_hello1,
                                        shadow_client_hello2, shadow_client_key_exchange)));
}

void fragmentLengthValidityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done,
                                  HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done)
{
  makeFragmentLenSymbolicNoFrag(hello_verify_request, 1);
  makeFragmentLenSymbolicNoFrag(server_hello, 3);
  makeFragmentLenSymbolicNoFrag(server_hello_done, 4);

  klee_assume(
      NEGATION(isFragmentLenValidClient(hello_verify_request, server_hello, server_hello_done, shadow_hello_verify_request,
                                        shadow_server_hello, shadow_server_hello_done)));
}

void offsetValidityNoFragServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  makeFragmentOffsetSymbolicNofrag(client_hello1, 0);
  makeFragmentOffsetSymbolicNofrag(client_hello2, 2);
  makeFragmentOffsetSymbolicNofrag(client_key_exchange, 5);

  klee_assume(
      NEGATION(isOffsetValidNoFragServer(client_hello1, client_hello2, client_key_exchange)));
}

void offsetValidityNoFragClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  makeFragmentOffsetSymbolicNofrag(hello_verify_request, 1);
  makeFragmentOffsetSymbolicNofrag(server_hello, 3);
  makeFragmentOffsetSymbolicNofrag(server_hello_done, 4);

  klee_assume(
      NEGATION(isOffsetValidNoFragClient(hello_verify_request, server_hello, server_hello_done)));
}

void fragLenMessageLenEqualityServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  makeFragmentLenSymbolicNoFrag(client_hello1, 0);
  makeFragmentLenSymbolicNoFrag(client_hello2, 2);
  makeFragmentLenSymbolicNoFrag(client_key_exchange, 5);
  makeMessageLenSymbolicNoFrag(client_hello1, 0);
  makeMessageLenSymbolicNoFrag(client_hello2, 2);
  makeMessageLenSymbolicNoFrag(client_key_exchange, 5);

  klee_assume(
      NEGATION(isFragmentLenMessageLenEqualServer(client_hello1, client_hello2, client_key_exchange)));
}

void fragLenMessageLenEqualityClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  makeFragmentLenSymbolicNoFrag(hello_verify_request, 1);
  makeFragmentLenSymbolicNoFrag(server_hello, 3);
  makeFragmentLenSymbolicNoFrag(server_hello_done, 4);
  makeMessageLenSymbolicNoFrag(hello_verify_request, 1);
  makeMessageLenSymbolicNoFrag(server_hello, 3);
  makeMessageLenSymbolicNoFrag(server_hello_done, 4);

  klee_assume(
      NEGATION(isFragmentLenMessageLenEqualClient(hello_verify_request, server_hello, server_hello_done)));
}

void byteContainedFragServer(CKEFRAG *client_key_exchange)
{
  makeFragmentOffsetSymbolicFrag(client_key_exchange, 5);
  makeFragmentLenSymbolicFrag(client_key_exchange, 5);

  klee_assume(client_key_exchange->fragment_offsetf1[0] == 0x00);
  klee_assume(client_key_exchange->fragment_offsetf1[1] == 0x00);

  klee_assume(client_key_exchange->fragment_offsetf2[0] == 0x00);
  klee_assume(client_key_exchange->fragment_offsetf2[1] == 0x00);

  klee_assume(client_key_exchange->fragment_lengthf1[0] == 0x00);
  klee_assume(client_key_exchange->fragment_lengthf1[1] == 0x00);

  klee_assume(client_key_exchange->fragment_lengthf2[0] == 0x00);
  klee_assume(client_key_exchange->fragment_lengthf2[1] == 0x00);

  unsigned int handshake_length = client_key_exchange->handshake_lengthf1[2] | client_key_exchange->handshake_lengthf1[1] << 8 | client_key_exchange->handshake_lengthf1[0] << 16;
  unsigned int b = klee_range(0, handshake_length, "byte");

  klee_assume(
      NEGATION(isByteContainedFragServer(client_key_exchange, b)));
}

void byteContainedFragClient(SHFRAG *server_hello)
{
  makeFragmentOffsetSymbolicFrag(server_hello, 3);
  makeFragmentLenSymbolicFrag(server_hello, 3);

  klee_assume(server_hello->fragment_offsetf1[0] == 0x00);
  klee_assume(server_hello->fragment_offsetf1[1] == 0x00);

  klee_assume(server_hello->fragment_offsetf2[0] == 0x00);
  klee_assume(server_hello->fragment_offsetf2[1] == 0x00);

  klee_assume(server_hello->fragment_lengthf1[0] == 0x00);
  klee_assume(server_hello->fragment_lengthf1[1] == 0x00);

  klee_assume(server_hello->fragment_lengthf2[0] == 0x00);
  klee_assume(server_hello->fragment_lengthf2[1] == 0x00);

  unsigned int handshake_length = server_hello->handshake_lengthf1[2] | server_hello->handshake_lengthf1[1] << 8 | server_hello->handshake_lengthf1[0] << 16; 
  unsigned int b = klee_range(0, handshake_length, "byte");

  klee_assume(
      NEGATION(isByteContainedFragClient(server_hello, b)));
}

void byteContainedFragServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2)
{
  makeFragmentOffsetSymbolicFrag(client_key_exchangef1, 51);
  makeFragmentOffsetSymbolicFrag(client_key_exchangef2, 52);

  makeFragmentLenSymbolicFrag(client_key_exchangef1, 51);
  makeFragmentLenSymbolicFrag(client_key_exchangef2, 52);

  // makeMessageLenSymbolicFrag(client_key_exchangef1, 51);
  // makeMessageLenSymbolicFrag(client_key_exchangef2, 52);

  klee_assume(client_key_exchangef1->fragment_offset[0] == 0x00);
  klee_assume(client_key_exchangef1->fragment_offset[1] == 0x00);

  klee_assume(client_key_exchangef2->fragment_offset[0] == 0x00);
  klee_assume(client_key_exchangef2->fragment_offset[1] == 0x00);

  klee_assume(client_key_exchangef1->fragment_length[0] == 0x00);
  klee_assume(client_key_exchangef1->fragment_length[1] == 0x00);

  klee_assume(client_key_exchangef2->fragment_length[0] == 0x00);
  klee_assume(client_key_exchangef2->fragment_length[1] == 0x00);

  // klee_assume(client_key_exchangef1->handshake_length[0] == 0x00);
  // klee_assume(client_key_exchangef1->handshake_length[1] == 0x00);

  // klee_assume(client_key_exchangef2->handshake_length[0] == 0x00);
  // klee_assume(client_key_exchangef2->handshake_length[1] == 0x00);
  
  unsigned int handshake_length = client_key_exchangef1->handshake_length[2] | client_key_exchangef1->handshake_length[1] << 8 | client_key_exchangef1->handshake_length[0] << 16;

  unsigned int b = klee_range(0, handshake_length, "byte");

  klee_assume(
      NEGATION(isByteContainedFragServerTiny(client_key_exchangef1, client_key_exchangef2, b)));
}

void byteContainedFragClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2)
{
  makeFragmentOffsetSymbolicFrag(server_hellof1, 31);
  makeFragmentOffsetSymbolicFrag(server_hellof2, 32);

  makeFragmentLenSymbolicFrag(server_hellof1, 31);
  makeFragmentLenSymbolicFrag(server_hellof2, 32);

  // makeMessageLenSymbolicFrag(server_hellof1, 31);
  // makeMessageLenSymbolicFrag(server_hellof2, 32);
  
  klee_assume(server_hellof1->fragment_offset[0] == 0x00);
  klee_assume(server_hellof1->fragment_offset[1] == 0x00);

  klee_assume(server_hellof2->fragment_offset[0] == 0x00);
  klee_assume(server_hellof2->fragment_offset[1] == 0x00);

  klee_assume(server_hellof1->fragment_length[0] == 0x00);
  klee_assume(server_hellof1->fragment_length[1] == 0x00);

  klee_assume(server_hellof2->fragment_length[0] == 0x00);
  klee_assume(server_hellof2->fragment_length[1] == 0x00);

  // klee_assume(server_hellof1->handshake_length[0] == 0x00);
  // klee_assume(server_hellof1->handshake_length[1] == 0x00);

  // klee_assume(server_hellof2->handshake_length[0] == 0x00);
  // klee_assume(server_hellof2->handshake_length[1] == 0x00);

  unsigned int handshake_length = server_hellof1->handshake_length[2] | server_hellof1->handshake_length[1] << 8 | server_hellof1->handshake_length[0] << 16; 
  unsigned int b = klee_range(0, handshake_length-1, "byte");

  klee_assume(
      NEGATION(isByteContainedFragClientTiny(server_hellof1, server_hellof2, b)));
}

void messageSequenceEqualityFragServer(CKEFRAG *client_key_exchange)
{
  makeMessageSequenceSymbolicFrag(client_key_exchange, 5);

  klee_assume(
      NEGATION(isMessageSequenceEqualFragServer(client_key_exchange)));
}

void messageSequenceEqualityFragClient(SHFRAG *server_hello)
{
  makeMessageSequenceSymbolicFrag(server_hello, 3);

  klee_assume(
      NEGATION(isMessageSequenceEqualFragClient(server_hello)));
}

void messageSequenceEqualityFragServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2)
{
  makeMessageSequenceSymbolicFrag(client_key_exchangef1, 51);
  makeMessageSequenceSymbolicFrag(client_key_exchangef2, 52);

  klee_assume(
      NEGATION(isMessageSequenceEqualFragServerTiny(client_key_exchangef1, client_key_exchangef2)));
}

void messageSequenceEqualityFragClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2)
{
  makeMessageSequenceSymbolicFrag(server_hellof1, 31);
  makeMessageSequenceSymbolicFrag(server_hellof2, 32);

  klee_assume(
      NEGATION(isMessageSequenceEqualFragClientTiny(server_hellof1, server_hellof2)));

}

void MessageLengthFragEqualityServer(CKEFRAG *client_key_exchange)
{
  makeMessageLenSymbolicFrag(client_key_exchange, 5);

  klee_assume(
      NEGATION(isMessageLenEqualFragServer(client_key_exchange)));
}

void MessageLengthFragEqualityClient(SHFRAG *server_hello)
{
  makeMessageLenSymbolicFrag(server_hello, 3);

  klee_assume(
      NEGATION(isMessageLenEqualFragClient(server_hello)));
}

void MessageLengthFragEqualityServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2)
{
  makeMessageLenSymbolicFrag(client_key_exchangef1, 51);
  makeMessageLenSymbolicFrag(client_key_exchangef2, 52);

  klee_assume(
      NEGATION(isMessageLenEqualFragServerTiny(client_key_exchangef1, client_key_exchangef2)));
}

void MessageLengthFragEqualityClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2)
{
  makeMessageLenSymbolicFrag(server_hellof1, 31);
  makeMessageLenSymbolicFrag(server_hellof2, 32);

  klee_assume(
      NEGATION(isMessageLenEqualFragClientTiny(server_hellof1, server_hellof2)));
}

/*

void fragmentLenRecordLenServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  makeFragmentLenSymbolicNoFrag(client_hello1, 0);
  makeFragmentLenSymbolicNoFrag(client_hello2, 2);
  makeFragmentLenSymbolicNoFrag(client_key_exchange, 5);
  makeRecordLenSymbolic(client_hello1, 0);
  makeRecordLenSymbolic(client_hello2, 2);
  makeRecordLenSymbolic(client_key_exchange, 5);

  klee_assume(
      NEGATION(isFragmentLenRecordLenRelatedServer(client_hello1, client_hello2, client_key_exchange)));
}

void fragmentLenRecordLenClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  makeFragmentLenSymbolicNoFrag(hello_verify_request, 1);
  makeFragmentLenSymbolicNoFrag(server_hello, 3);
  makeFragmentLenSymbolicNoFrag(server_hello_done, 4);
  makeRecordLenSymbolic(hello_verify_request, 1);
  makeRecordLenSymbolic(server_hello, 3);
  makeRecordLenSymbolic(server_hello_done, 4);

  klee_assume(
      NEGATION(isFragmentLenRecordLenRelatedClient(hello_verify_request, server_hello, server_hello_done)));
}

void messageLenRecordlenServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange)
{
  makeMessageLenSymbolicNoFrag(client_hello1, 0);
  makeMessageLenSymbolicNoFrag(client_hello2, 2);
  makeMessageLenSymbolicNoFrag(client_key_exchange, 5);
  makeRecordLenSymbolic(client_hello1, 0);
  makeRecordLenSymbolic(client_hello2, 2);
  makeRecordLenSymbolic(client_key_exchange, 5);

  klee_assume(
      NEGATION(isMessageLenRecordLenRelatedServer(client_hello1, client_hello2, client_key_exchange)));
}

void messageLenRecordlenClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done)
{
  makeMessageLenSymbolicNoFrag(hello_verify_request, 1);
  makeMessageLenSymbolicNoFrag(server_hello, 3);
  makeMessageLenSymbolicNoFrag(server_hello_done, 4);
  makeRecordLenSymbolic(hello_verify_request, 1);
  makeRecordLenSymbolic(server_hello, 3);
  makeRecordLenSymbolic(server_hello_done, 4);

  klee_assume(
      NEGATION(isMessageLenRecordLenRelatedClient(hello_verify_request, server_hello, server_hello_done)));
}

*/

void handshakeVersionServer(CH0 *client_hello1, CH2 *client_hello2)
{
  makeHandshakeVersionSymbolic(client_hello1, 0);
  makeHandshakeVersionSymbolic(client_hello2, 2);

  klee_assume(
      NEGATION(isHandshakeVersionValidServer(client_hello1, client_hello2)));
}

void handshakeVersionClient(HVR *hello_verify_request, SH *server_hello)
{
  makeHandshakeVersionSymbolic(hello_verify_request, 1);
  makeHandshakeVersionSymbolic(server_hello, 3);

  klee_assume(
      NEGATION(isHandshakeVersionValidClient(hello_verify_request, server_hello)));
}

void cookieLengthServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                        CH2 *shadow_client_hello2)
{
  makeCookieLengthSymbolic(client_hello1, 0);
  makeCookieLengthSymbolic(client_hello2, 2);

  klee_assume(
      NEGATION(isCookieLengthValidServer(client_hello1, client_hello2, shadow_client_hello1,
                                         shadow_client_hello2)));
}

void cookieLengthClient(HVR *hello_verify_request, HVR *shadow_hello_verify_request)
{
  makeCookieLengthSymbolic(hello_verify_request, 1);

  klee_assume(
      NEGATION(isCookieLengthValidClient(hello_verify_request, shadow_hello_verify_request)));
}

void sessionIDLengthServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                           CH2 *shadow_client_hello2)
{
  makeSessionIDLengthSymbolic(client_hello1, 0);
  makeSessionIDLengthSymbolic(client_hello2, 2);

  klee_assume(
      NEGATION(isSessionIDLengthValidServer(client_hello1, client_hello2, shadow_client_hello1,
                                            shadow_client_hello2)));
}

void sessionIDLengthClient(SH *server_hello, SH *shadow_server_hello)
{
  makeSessionIDLengthSymbolic(server_hello, 3);

  klee_assume(
      NEGATION(isSessionIDLengthValidClient(server_hello, shadow_server_hello)));
}

void extensionLengthValidityServer(CH0 *client_hello1, CH2 *client_hello2, CH0 *shadow_client_hello1,
                           CH2 *shadow_client_hello2)
{
  makeExtensionLengthSymbolic(client_hello1, 0);
  makeExtensionLengthSymbolic(client_hello2, 2);

  klee_assume(
    NEGATION(isExtensionLengthValidServer(client_hello1, client_hello2, shadow_client_hello1, shadow_client_hello2))
  );
  
}

void extensionLengthValidityClient(SH *server_hello, SH *shadow_server_hello)
{
  makeExtensionLengthSymbolic(server_hello, 3);

  klee_assume(
    NEGATION(isExtensionLengthValidClient(server_hello, shadow_server_hello))
  );
}
////////////
/*
void offsetValidityFragServer(CKEFRAG *client_key_exchange, CKEFRAG *shadow_client_key_exchange)
{
  makeFragmentOffsetSymbolicFrag(client_key_exchange, 5);
  klee_assume(client_key_exchange->fragment_offsetf1[0] == 0x00);
  klee_assume(client_key_exchange->fragment_offsetf1[1] == 0x00);
  klee_assume(client_key_exchange->fragment_offsetf2[0] == 0x00);
  klee_assume(client_key_exchange->fragment_offsetf2[1] == 0x00);

  klee_assume(
      NEGATION(isOffsetValidFragServer(client_key_exchange, shadow_client_key_exchange)));
}

void offsetValidityFragClient(SHFRAG *server_hello, SHFRAG *shadow_server_hello)
{
  makeFragmentOffsetSymbolicFrag(server_hello, 3);
  klee_assume(server_hello->fragment_offsetf1[0] == 0x00);
  klee_assume(server_hello->fragment_offsetf1[1] == 0x00);
  klee_assume(server_hello->fragment_offsetf2[0] == 0x00);
  klee_assume(server_hello->fragment_offsetf2[1] == 0x00);
  klee_assume(
      NEGATION(isOffsetValidFragClient(server_hello, shadow_server_hello)));
}

void fragLenMessageLenRelationServer(CKEFRAG *client_key_exchange)
{

  makeFragmentLenSymbolicFrag(client_key_exchange, 5);
  makeMessageLenSymbolicFrag(client_key_exchange, 5);

  klee_assume(
      NEGATION(isFragmentLenMessageLenRelatedServer(client_key_exchange)));
}

void fragLenMessageLenRelationClient(SHFRAG *server_hello)
{
  makeFragmentLenSymbolicFrag(server_hello, 3);
  makeMessageLenSymbolicFrag(server_hello, 3);

  klee_assume(
      NEGATION(isFragmentLenMessageLenRelatedClient(server_hello)));
}
*/
/*

void offsetValidityFragServerTiny(CKEf1 *client_key_exchangef1, CKEf1 *shadow_client_key_exchangef1,
                                  CKEf2 *client_key_exchangef2, CKEf2 *shadow_client_key_exchangef2)
{
  makeFragmentOffsetSymbolicFrag(client_key_exchangef1, 51);
  makeFragmentOffsetSymbolicFrag(client_key_exchangef2, 52);

  klee_assume(client_key_exchangef1->fragment_offset[0] == 0x00);
  klee_assume(client_key_exchangef1->fragment_offset[1] == 0x00);

  klee_assume(client_key_exchangef2->fragment_offset[0] == 0x00);
  klee_assume(client_key_exchangef2->fragment_offset[1] == 0x00);

  klee_assume(
      NEGATION(isOffsetValidFragServerTiny(client_key_exchangef1, shadow_client_key_exchangef1, client_key_exchangef2,
                                           shadow_client_key_exchangef2)));
}

void offsetValidityFragClientTiny(SHf1 *server_hellof1, SHf1 *shadow_server_hellof1,
                                  SHf2 *server_hellof2, SHf2 *shadow_server_hellof2)
{
  makeFragmentOffsetSymbolicFrag(server_hellof1, 31);
  makeFragmentOffsetSymbolicFrag(server_hellof2, 32);

  klee_assume(server_hellof1->fragment_offset[0] == 0x00);
  klee_assume(server_hellof1->fragment_offset[1] == 0x00);

  klee_assume(server_hellof2->fragment_offset[0] == 0x00);
  klee_assume(server_hellof2->fragment_offset[1] == 0x00);

  klee_assume(
      NEGATION(isOffsetValidFragClientTiny(server_hellof1, shadow_server_hellof1, server_hellof2,
                                           shadow_server_hellof2)));
}

void fragLenMessageLenRelationServerTiny(CKEf1 *client_key_exchangef1, CKEf2 *client_key_exchangef2)
{
  makeFragmentLenSymbolicFrag(client_key_exchangef1, 51);
  makeFragmentLenSymbolicFrag(client_key_exchangef2, 52);
  makeMessageLenSymbolicFrag(client_key_exchangef1, 51);
  makeMessageLenSymbolicFrag(client_key_exchangef2, 52);

  klee_assume(
    NEGATION(isFragmentLenMessageLenRelatedServerTiny(client_key_exchangef1, client_key_exchangef2))
  );
}

void fragLenMessageLenRelationClientTiny(SHf1 *server_hellof1, SHf2 *server_hellof2)
{
  makeFragmentLenSymbolicFrag(server_hellof1, 31);
  makeFragmentLenSymbolicFrag(server_hellof2, 32);
  makeMessageLenSymbolicFrag(server_hellof1, 31);
  makeMessageLenSymbolicFrag(server_hellof2, 32);

  klee_assume(
    NEGATION(isFragmentLenMessageLenRelatedClientTiny(server_hellof1, server_hellof2))
  );
}

void allConstraintsServer(CH0 *client_hello1, CH2 *client_hello2, CKE *client_key_exchange, CCS *client_change_cipher,
                          CH0 *shadow_client_hello1, CH2 *shadow_client_hello2, CKE *shadow_client_key_exchange, CCS *shadow_client_change_cipher)
{
  makeEpochSymbolic(client_hello1, 0);
  makeEpochSymbolic(client_hello2, 2);
  makeEpochSymbolic(client_key_exchange, 5);
  makeEpochSymbolic(client_change_cipher, 6);
  //
  makeSequenceSymbolic(client_hello1, 0);
  makeSequenceSymbolic(client_hello2, 2);
  makeSequenceSymbolic(client_key_exchange, 5);
  makeSequenceSymbolic(client_change_cipher, 6);
  //
  makeMessageSequenceSymbolic(client_hello1, 0);
  makeMessageSequenceSymbolic(client_hello2, 2);
  makeMessageSequenceSymbolic(client_key_exchange, 5);
  //
  makeFragmentOffsetSymbolicNofrag(client_hello1, 0);
  makeFragmentOffsetSymbolicNofrag(client_hello2, 2);
  makeFragmentOffsetSymbolicNofrag(client_key_exchange, 5);
  //
  makeFragmentLenSymbolicNoFrag(client_hello1, 0);
  makeFragmentLenSymbolicNoFrag(client_hello2, 2);
  makeFragmentLenSymbolicNoFrag(client_key_exchange, 5);
  //
  makeMessageLenSymbolicNoFrag(client_hello1, 0);
  makeMessageLenSymbolicNoFrag(client_hello2, 2);
  makeMessageLenSymbolicNoFrag(client_key_exchange, 5);
  //
  makeRecordLenSymbolic(client_hello1, 0);
  makeRecordLenSymbolic(client_hello2, 2);
  makeRecordLenSymbolic(client_key_exchange, 5);
  //
  makeRecordVersionSymbolic(client_hello2, 2);
  makeRecordVersionSymbolic(client_key_exchange, 5);
  makeRecordVersionSymbolic(client_change_cipher, 6);
  //
  makeHandshakeVersionSymbolic(client_hello1, 0);
  makeHandshakeVersionSymbolic(client_hello2, 2);
  //
  makeContentTypeSybmolic(client_hello1, 0);
  makeContentTypeSybmolic(client_hello2, 2);
  makeContentTypeSybmolic(client_key_exchange, 5);
  makeContentTypeSybmolic(client_change_cipher, 6);
  //
  makeCookieLengthSymbolic(client_hello1, 0);
  makeCookieLengthSymbolic(client_hello2, 2);
  //
  makeSessionIDLengthSymbolic(client_hello1, 0);
  makeSessionIDLengthSymbolic(client_hello2, 2);
  //
  klee_assume(
      NEGATION(isEpochValidServer(client_hello1, client_hello2, client_key_exchange, client_change_cipher))
          OR
              NEGATION(isRecordSequenceUniqueServer(client_hello2, client_key_exchange, client_change_cipher))
                  OR
                      NEGATION(isRecordSequenceValidServer(client_hello2, client_key_exchange, client_change_cipher))
                          OR
                              NEGATION(isMessageSequenceValidServer(client_hello1, client_hello2, client_key_exchange))
                                  OR
                                      NEGATION(isOffsetValidNoFragServer(client_hello1, client_hello2, client_key_exchange))
                                          OR
                                              NEGATION(isFragmentLenMessageLenEqualServer(client_hello1, client_hello2, client_key_exchange))
                                                  OR
                                                      NEGATION(isFragmentLenRecordLenRelatedServer(client_hello1, client_hello2, client_key_exchange))
                                                          OR
                                                              NEGATION(isMessageLenRecordLenRelatedServer(client_hello1, client_hello2, client_key_exchange))
                                                                  OR
                                                                      NEGATION(isFragmentLenValidServer(client_hello1, client_hello2, client_key_exchange, shadow_client_hello1,
                                                                                                        shadow_client_hello2, shadow_client_key_exchange))
                                                                          OR
                                                                              NEGATION(isMessageLenValidServer(client_hello1, client_hello2, client_key_exchange, shadow_client_hello1,
                                                                                                               shadow_client_hello2, shadow_client_key_exchange))
                                                                                  OR
                                                                                      NEGATION(isRecordLenValidServer(client_hello1, client_hello2, client_key_exchange, client_change_cipher,
                                                                                                                      shadow_client_hello1, shadow_client_hello2, shadow_client_key_exchange, shadow_client_change_cipher))
                                                                                          OR
                                                                                              NEGATION(isRecordVersionValidServer(client_hello2, client_key_exchange, client_change_cipher))
                                                                                                  OR
                                                                                                      NEGATION(isHandshakeVersionValidServer(client_hello1, client_hello2))
                                                                                                          OR
                                                                                                              NEGATION(isContentTypeValidServer(client_hello1, client_hello2, client_key_exchange, client_change_cipher))
                                                                                                                  OR
                                                                                                                      NEGATION(isCookieLengthValidServer(client_hello1, client_hello2, shadow_client_hello1, shadow_client_hello2))
                                                                                                                          OR
                                                                                                                              NEGATION(isSessionIDLengthValidServer(client_hello1, client_hello2, shadow_client_hello1, shadow_client_hello2)));
}

void allConstraintsClient(HVR *hello_verify_request, SH *server_hello, SHD *server_hello_done, CCS *server_change_cipher,
                          HVR *shadow_hello_verify_request, SH *shadow_server_hello, SHD *shadow_server_hello_done,
                          CCS *shadow_server_change_cipher)
{
  makeEpochSymbolic(hello_verify_request, 1);
  makeEpochSymbolic(server_hello, 3);
  makeEpochSymbolic(server_hello_done, 4);
  makeEpochSymbolic(server_change_cipher, 8);
  //
  makeSequenceSymbolic(hello_verify_request, 1);
  makeSequenceSymbolic(server_hello, 3);
  makeSequenceSymbolic(server_hello_done, 4);
  makeSequenceSymbolic(server_change_cipher, 8);
  //
  makeMessageSequenceSymbolic(hello_verify_request, 1);
  makeMessageSequenceSymbolic(server_hello, 3);
  makeMessageSequenceSymbolic(server_hello_done, 4);
  //
  makeFragmentOffsetSymbolicNofrag(hello_verify_request, 1);
  makeFragmentOffsetSymbolicNofrag(server_hello, 3);
  makeFragmentOffsetSymbolicNofrag(server_hello_done, 4);
  //
  makeFragmentLenSymbolicNoFrag(hello_verify_request, 1);
  makeFragmentLenSymbolicNoFrag(server_hello, 3);
  makeFragmentLenSymbolicNoFrag(server_hello_done, 4);
  //
  makeMessageLenSymbolicNoFrag(hello_verify_request, 1);
  makeMessageLenSymbolicNoFrag(server_hello, 3);
  makeMessageLenSymbolicNoFrag(server_hello_done, 4);
  //
  makeRecordLenSymbolic(hello_verify_request, 1);
  makeRecordLenSymbolic(server_hello, 3);
  makeRecordLenSymbolic(server_hello_done, 4);
  //
  makeRecordVersionSymbolic(hello_verify_request, 1);
  makeRecordVersionSymbolic(server_hello, 3);
  makeRecordVersionSymbolic(server_hello_done, 4);
  makeRecordVersionSymbolic(server_change_cipher, 8);
  //
  makeHandshakeVersionSymbolic(hello_verify_request, 1);
  makeHandshakeVersionSymbolic(server_hello, 3);
  //
  makeContentTypeSybmolic(hello_verify_request, 1);
  makeContentTypeSybmolic(server_hello, 3);
  makeContentTypeSybmolic(server_hello_done, 4);
  makeContentTypeSybmolic(server_change_cipher, 8);
  //
  makeCookieLengthSymbolic(hello_verify_request, 1);
  //
  makeSessionIDLengthSymbolic(server_hello, 3);
  //
  klee_assume(
      NEGATION(isEpochValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher))
          OR
              NEGATION(isRecordSequenceUniqueClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher))
                  OR
                      NEGATION(isRecordSequenceValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher))
                          OR
                              NEGATION(isMessageSequenceValidClient(hello_verify_request, server_hello, server_hello_done))
                                  OR
                                      NEGATION(isOffsetValidNoFragClient(hello_verify_request, server_hello, server_hello_done))
                                          OR
                                              NEGATION(isFragmentLenMessageLenEqualClient(hello_verify_request, server_hello, server_hello_done))
                                                  OR
                                                      NEGATION(isFragmentLenRecordLenRelatedClient(hello_verify_request, server_hello, server_hello_done))
                                                          OR
                                                              NEGATION(isMessageLenRecordLenRelatedClient(hello_verify_request, server_hello, server_hello_done))
                                                                  OR
                                                                      NEGATION(isFragmentLenValidClient(hello_verify_request, server_hello, server_hello_done, shadow_hello_verify_request,
                                                                                                        shadow_server_hello, shadow_server_hello_done))
                                                                          OR
                                                                              NEGATION(isMessageLenValidClient(hello_verify_request, server_hello, server_hello_done, shadow_hello_verify_request,
                                                                                                               shadow_server_hello, shadow_server_hello_done))
                                                                                  OR
                                                                                      NEGATION(isRecordLenValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher,
                                                                                                                      shadow_hello_verify_request, shadow_server_hello, shadow_server_hello_done, shadow_server_change_cipher))
                                                                                          OR
                                                                                              NEGATION(isRecordVersionValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher))
                                                                                                  OR
                                                                                                      NEGATION(isHandshakeVersionValidClient(hello_verify_request, server_hello))
                                                                                                          OR
                                                                                                              NEGATION(isContentTypeValidClient(hello_verify_request, server_hello, server_hello_done, server_change_cipher))
                                                                                                                  OR
                                                                                                                      NEGATION(isCookieLengthValidClient(hello_verify_request, shadow_hello_verify_request))
                                                                                                                          OR
                                                                                                                              NEGATION(isSessionIDLengthValidClient(server_hello, shadow_server_hello)));
}

*/
//////////////////////////////////////////////////////////////////////

void symbolicClientHello1(CH0 *client_hello1, CH0 *shadow_client_hello1)
{
  klee_make_symbolic(client_hello1, sizeof(CH0), "client_hello1");

  for (unsigned long i = 0; i < sizeof(client_hello1->random); i++)
  {
    klee_assume(client_hello1->random[i] == shadow_client_hello1->random[i]);
  }

  klee_assume(client_hello1->cookie_length == shadow_client_hello1->cookie_length);

#if MbedTLS | OpenSSL | OldOpenSSL

  klee_assume(client_hello1->session_id_length == shadow_client_hello1->session_id_length);

  klee_assume(client_hello1->extension_length[0] == shadow_client_hello1->extension_length[0]);
  klee_assume(client_hello1->extension_length[1] == shadow_client_hello1->extension_length[1]);

  for (unsigned long i = 0; i < sizeof(client_hello1->extensions); i++)
  {
    klee_assume(client_hello1->extensions[i] == shadow_client_hello1->extensions[i]);
  }
#endif
}

void symbolicClientHello2(CH2 *client_hello2, CH2 *shadow_client_hello2)
{
  klee_make_symbolic(client_hello2, sizeof(CH2), "client_hello2");

  for (unsigned long i = 0; i < sizeof(client_hello2->random); i++)
  {
    klee_assume(client_hello2->random[i] == shadow_client_hello2->random[i]);
  }
  klee_assume(client_hello2->cookie_length == shadow_client_hello2->cookie_length);

  for (unsigned long i = 0; i < sizeof(client_hello2->cookie); i++)
  {
    klee_assume(client_hello2->cookie[i] == shadow_client_hello2->cookie[i]);
  }

#if MbedTLS | OpenSSL | OldOpenSSL

  klee_assume(client_hello2->session_id_length == shadow_client_hello2->session_id_length);

  klee_assume(client_hello2->extension_length[0] == shadow_client_hello2->extension_length[0]);
  klee_assume(client_hello2->extension_length[1] == shadow_client_hello2->extension_length[1]);

  for (unsigned long i = 0; i < sizeof(client_hello2->extensions); i++)
  {
    klee_assume(client_hello2->extensions[i] == shadow_client_hello2->extensions[i]);
  }
#endif
}

void symbolicClientKeyExchange(CKE *client_key_exchange, CKE *shadow_client_key_exchange)
{
  klee_make_symbolic(client_key_exchange, sizeof(CKE), "client_key_exchange");
}

void symbolicClientChangeCipher(CCS *client_change_cipher, CCS *shadow_client_change_cipher)
{
  klee_make_symbolic(client_change_cipher, sizeof(CCS), "client_change_cipher");
}

void symbolicClientFinished(FIN *client_finished, FIN *shadow_client_finished)
{
  klee_make_symbolic(client_finished, sizeof(FIN), "client_finished");

  for (unsigned long i = 0; i < sizeof(client_finished->encrypted_content); i++)
  {
    klee_assume(client_finished->encrypted_content[i] == shadow_client_finished->encrypted_content[i]);
  }
}

void symbolicHelloVerifyRequest(HVR *hello_verify_request, HVR *shadow_hello_verify_request)
{
  klee_make_symbolic(hello_verify_request, sizeof(HVR), "hello_verify_request");

  klee_assume(hello_verify_request->cookie_length == shadow_hello_verify_request->cookie_length);
  for (unsigned long i = 0; i < sizeof(hello_verify_request->cookie); i++)
  {
    klee_assume(hello_verify_request->cookie[i] == shadow_hello_verify_request->cookie[i]);
  }
}

void symbolicServerHello(SH *server_hello, SH *shadow_server_hello)
{
  klee_make_symbolic(server_hello, sizeof(SH), "server_hello");
  for (unsigned long i = 0; i < sizeof(server_hello->random); i++)
  {
    klee_assume(server_hello->random[i] == shadow_server_hello->random[i]);
  }

#if MbedTLS | OpenSSL | OldOpenSSL

  klee_assume(server_hello->session_id_length == shadow_server_hello->session_id_length);

  for (unsigned long i = 0; i < sizeof(server_hello->session_id); i++)
  {
    klee_assume(server_hello->session_id[i] == shadow_server_hello->session_id[i]);
  }

  klee_assume(server_hello->extension_length[0] == shadow_server_hello->extension_length[0]);
  klee_assume(server_hello->extension_length[1] == shadow_server_hello->extension_length[1]);

  for (unsigned long i = 0; i < sizeof(server_hello->extensions); i++)
  {
    klee_assume(server_hello->extensions[i] == server_hello->extensions[i]);
  }
#endif
}

void symbolicServerHelloDone(SHD *server_hello_done, SHD *shadow_server_hello_done)
{
  klee_make_symbolic(server_hello_done, sizeof(SHD), "server_hello_done");
}

void symbolicServerChangeCipher(CCS *server_change_cipher, CCS *shadow_server_change_cipher)
{
  klee_make_symbolic(server_change_cipher, sizeof(CCS), "server_change_cipher");
}

void symbolicServerFinished(FIN *server_finished, FIN *shadow_server_finished)
{
  klee_make_symbolic(server_finished, sizeof(FIN), "server_finished");
  for (unsigned long i = 0; i < sizeof(server_finished->encrypted_content); i++)
  {
    klee_assume(server_finished->encrypted_content[i] == shadow_server_finished->encrypted_content[i]);
  }
}
