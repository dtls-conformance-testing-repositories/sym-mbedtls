
# To compile on SunOS: add "-lsocket -lnsl" to LDFLAGS
# To compile with PKCS11: add "-lpkcs11-helper" to LDFLAGS

KLEE_BUILD_PATH=/home/hooman/Repos/klee-augmented/build

ifdef sym
CC=wllvm
CFLAGS	?= -fPIC -O1 -g -Xclang -disable-llvm-passes -D__NO_STRING_INLINES  -D_FORTIFY_SOURCE=0 -U__OPTIMIZE__
else
CFLAGS	?= -O0 -g
LOCAL_CFLAGS += -g3
endif

WARNING_CFLAGS ?= -Wall -Wextra
WARNING_CXXFLAGS ?= -Wall -Wextra
LDFLAGS ?=

LOCAL_CFLAGS = $(WARNING_CFLAGS) -I ../library -I../include -D_FILE_OFFSET_BITS=64 -I /home/hooman/Repos/klee/include
LOCAL_CXXFLAGS = $(WARNING_CXXFLAGS) -I../include -D_FILE_OFFSET_BITS=64
LOCAL_LDFLAGS = -L../library 			\
		-L$(KLEE_BUILD_PATH)/lib \
		-lmbedtls$(SHARED_SUFFIX)	\
		-lmbedx509$(SHARED_SUFFIX)	\
		-lmbedcrypto$(SHARED_SUFFIX) \
		$(KLEE_BUILD_PATH)/lib/libkleeRuntest.so

include ../3rdparty/Makefile.inc
LOCAL_CFLAGS+=$(THIRDPARTY_INCLUDES)

ifndef SHARED
DEP=../library/libmbedcrypto.a ../library/libmbedx509.a ../library/libmbedtls.a
else
DEP=../library/libmbedcrypto.$(DLEXT) ../library/libmbedx509.$(DLEXT) ../library/libmbedtls.$(DLEXT)
endif

ifdef DEBUG
LOCAL_CFLAGS += -g3
endif

# if we're running on Windows, build for Windows
ifdef WINDOWS
WINDOWS_BUILD=1
endif

ifdef WINDOWS_BUILD
DLEXT=dll
EXEXT=.exe
LOCAL_LDFLAGS += -lws2_32
ifdef SHARED
SHARED_SUFFIX=.$(DLEXT)
endif
else
DLEXT ?= so
EXEXT=
SHARED_SUFFIX=
endif

# Zlib shared library extensions:
ifdef ZLIB
LOCAL_LDFLAGS += -lz
endif

APPS = \
	ssl/dtls-fuzz$(EXEXT) \
	ssl/dtls_client$(EXEXT) \
	ssl/dtls_server$(EXEXT) \
	ssl/ssl_client1$(EXEXT) \
	ssl/ssl_client2$(EXEXT) \
	ssl/ssl_server$(EXEXT) \
	ssl/ssl_server2$(EXEXT) \
# End of APPS

ifdef PTHREAD
APPS +=	ssl/ssl_pthread_server$(EXEXT)
endif

ifdef TEST_CPP
APPS += test/cpp_dummy_build$(EXEXT)
endif

EXTRA_GENERATED =

.SILENT:

.PHONY: all clean list fuzz

all: $(APPS)
ifndef WINDOWS
# APPS doesn't include the fuzzing programs, which aren't "normal"
# sample or test programs, and don't build with MSVC which is
# warning about fopen
endif

fuzz:
	$(MAKE) -C fuzz THIRDPARTY_INCLUDES=$(THIRDPARTY_INCLUDES)

$(DEP):
	$(MAKE) -C ../library

ifdef WINDOWS
EXTRA_GENERATED += psa\psa_constant_names_generated.c
else
EXTRA_GENERATED += psa/psa_constant_names_generated.c
endif

psa/psa_constant_names$(EXEXT): psa/psa_constant_names_generated.c
psa/psa_constant_names_generated.c: ../scripts/generate_psa_constants.py ../include/psa/crypto_values.h ../include/psa/crypto_extra.h
	../scripts/generate_psa_constants.py

ssl/dtls-fuzz$(EXEXT): ssl/dtls-fuzz.c $(DEP)
	echo "  CC    ssl/dtls-fuzz.c"
	$(CC) $(LOCAL_CFLAGS) $(CFLAGS) ssl/dtls-fuzz.c  $(LOCAL_LDFLAGS) $(LDFLAGS) -o $@
	
ssl/dtls_client$(EXEXT): ssl/dtls_client.c $(DEP)
	echo "  CC    ssl/dtls_client.c"
	$(CC) $(LOCAL_CFLAGS) $(CFLAGS) ssl/dtls_client.c  $(LOCAL_LDFLAGS) $(LDFLAGS) -o $@

ssl/dtls_server$(EXEXT): ssl/dtls_server.c $(DEP)
	echo "  CC    ssl/dtls_server.c"
	$(CC) $(LOCAL_CFLAGS) $(CFLAGS) ssl/dtls_server.c  $(LOCAL_LDFLAGS) $(LDFLAGS) -o $@

ssl/ssl_client1$(EXEXT): ssl/ssl_client1.c $(DEP)
	echo "  CC    ssl/ssl_client1.c"
	$(CC) $(LOCAL_CFLAGS) $(CFLAGS) ssl/ssl_client1.c  $(LOCAL_LDFLAGS) $(LDFLAGS) -o $@

ssl/ssl_client2$(EXEXT): ssl/ssl_client2.c test/query_config.c $(DEP)
	echo "  CC    ssl/ssl_client2.c"
	$(CC) $(LOCAL_CFLAGS) $(CFLAGS) ssl/ssl_client2.c test/query_config.c $(LOCAL_LDFLAGS) $(LDFLAGS) -o $@

ssl/ssl_server$(EXEXT): ssl/ssl_server.c $(DEP)
	echo "  CC    ssl/ssl_server.c"
	$(CC) $(LOCAL_CFLAGS) $(CFLAGS) ssl/ssl_server.c   $(LOCAL_LDFLAGS) $(LDFLAGS) -o $@

ssl/ssl_server2$(EXEXT): ssl/ssl_server2.c test/query_config.c $(DEP)
	echo "  CC    ssl/ssl_server2.c"
	$(CC) $(LOCAL_CFLAGS) $(CFLAGS) ssl/ssl_server2.c test/query_config.c $(LOCAL_LDFLAGS) $(LDFLAGS) -o $@

clean:
ifndef WINDOWS
	rm -f $(APPS)
	-rm -f ssl/ssl_pthread_server$(EXEXT)
	rm -f $(EXTRA_GENERATED)
	-rm -f test/cpp_dummy_build$(EXEXT)
else
	if exist *.o del /Q /F *.o
	if exist *.exe del /Q /F *.exe
	del /S /Q /F $(EXTRA_GENERATED)
endif
	$(MAKE) -C fuzz clean

list:
	echo $(APPS)
