echo -e "\e[32m[+] - Cleaning the project directory"
echo -e "\n"

rm -rf dtls-fuzz.bc
cd ../../

sym=1 make

cd programs/ssl

export CPATH=/home/$(whoami)/Repos/klee/include

echo -e "\e[32m[+] - Preparing the bitcode"
echo -e "\n"

extract-bc ./dtls-fuzz

echo -e "\n\e[32m[+] - Compilation has been successfuly completed!\n"

