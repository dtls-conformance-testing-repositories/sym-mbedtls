/*
 *  Sets up a DTLS server and a DTLS client instance and lets the instances
 *  talk to each other.
 *  The purpose of this program is to allow for testing the mbed TLS using Symbex
 * 
 */

#include "mbedtls/config.h"
#include "mbedtls/platform.h"

#include <stdlib.h>
#include <string.h>

#include "mbedtls/net.h"

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <netdb.h>
#include <errno.h>
#include <getopt.h>
#include <stdbool.h>

#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/certs.h"
#include "mbedtls/x509.h"
#include "mbedtls/ssl.h"
#include "mbedtls/net.h"
#include "mbedtls/error.h"
#include "mbedtls/debug.h"
#include "mbedtls/net.h"
#include "mbedtls/ssl_cache.h"
#include "mbedtls/ssl_cookie.h"
#include "mbedtls/timing.h"

#pragma clang diagnostic ignored "-Wunused-parameter"

/* Symbolic Execution Variables */
#include "records.h"
int symex = 0;

#if ISSYM

#include <klee/klee.h>
#include <symbolic.h>

#endif

#define DEBUG_LEVEL 1

#define MAX_FILENAME_LEN 100

char base_name[MAX_FILENAME_LEN], file_name[MAX_FILENAME_LEN], rule[50];

#define MAX_HANDSHAKE_STEPS (sizeof(client_steps) / sizeof(client_steps[0]))

/* Store sent messages in files for fuzzing. */
#if !defined(PACKET_FILE_PREFIX)
#define PACKET_FILE_PREFIX ""
#endif

static int dump_mode = 0;
bool isFragmented = 0;

static size_t packet_count = 0;

const char *pers = "dtls_server";

#define BUF_SIZE (100000)

static unsigned char server_send_buf[BUF_SIZE];
static size_t server_send_off = 0;
static size_t server_recv_off = 0;

static unsigned char client_send_buf[BUF_SIZE];
static size_t client_send_off = 0;
static size_t client_recv_off = 0;

static unsigned char *shared_buf = NULL;
static size_t *send_off = NULL;
static size_t *recv_off = NULL;

int client_steps[] = {2, 2, 10, 3};
int server_steps[] = {3, 8, 8, 0};

/* Handshake step counter */
size_t step = 1;
size_t symstep = 0;

/* Buffers for different records */
static unsigned char CH0_buf[CH0_size];
static unsigned char shadow_CH0[CH0_size];
static unsigned char HVR_buf[HVR_size];
static unsigned char shadow_HVR[HVR_size];
static unsigned char CH2_buf[CH2_size];
static unsigned char shadow_CH2[CH2_size];
static unsigned char SH_buf[SH_unfrag_size];
static unsigned char shadow_SH[SH_unfrag_size];
static unsigned char SHD_buf[SHD_size];
static unsigned char shadow_SHD[SHD_size];
static unsigned char CKE_buf[CKE_unfrag_size];
static unsigned char shadow_CKE[CKE_unfrag_size];
static unsigned char CCC_buf[CCS_size];
static unsigned char shadow_CCC[CCS_size];
static unsigned char CFI_buf[FIN_size];
static unsigned char shadow_CFI[FIN_size];
static unsigned char SCC_buf[CCS_size];
static unsigned char shadow_SCC[CCS_size];
static unsigned char SFI_buf[FIN_size];
static unsigned char shadow_SFI[FIN_size];

// Fragmentation
uint8_t CKE_buffrag[CKE_frag_size];
uint8_t shadow_CKE_buffrag[CKE_frag_size];
uint8_t SH_buffrag[SH_frag_size];
uint8_t shadow_SH_buffrag[SH_frag_size];

///////////////////
CH0 client_hello1;
CH0 shadow_client_hello1;
CH2 client_hello2;
CH2 shadow_client_hello2;
CKE client_key_exchange;
CKE shadow_client_key_exchange;
CKEFRAG client_key_exchange_frag;
CKEFRAG shadow_client_key_exchange_frag;
HVR hello_verify_request;
HVR shadow_hello_verify_request;
SH server_hello;
SH shadow_server_hello;
SHFRAG server_hello_frag;
SHFRAG shadow_server_hello_frag;
SHD server_hello_done;
SHD shadow_server_hello_done;
CCS client_change_cipher;
CCS shadow_client_change_cipher;
FIN client_finished;
FIN shadow_client_finished;
CCS server_change_cipher;
CCS shadow_server_change_cipher;
FIN server_finished;
FIN shadow_server_finished;

#define HEX2NUM(c)                         \
    do                                     \
    {                                      \
        if ((c) >= '0' && (c) <= '9')      \
            (c) -= '0';                    \
        else if ((c) >= 'a' && (c) <= 'f') \
            (c) -= 'a' - 10;               \
        else if ((c) >= 'A' && (c) <= 'F') \
            (c) -= 'A' - 10;               \
        else                               \
            return (-1);                   \
    } while (0)

/*
 * Convert a hex string to bytes.
 * Return 0 on success, -1 on error.
 */
int unhexify(unsigned char *output, const char *input, size_t *olen)
{
    unsigned char c;
    size_t j;

    *olen = strlen(input);
    if (*olen % 2 != 0 || *olen / 2 > MBEDTLS_PSK_MAX_LEN)
        return (-1);
    *olen /= 2;

    for (j = 0; j < *olen * 2; j += 2)
    {
        c = input[j];
        HEX2NUM(c);
        output[j / 2] = c << 4;

        c = input[j + 1];
        HEX2NUM(c);
        output[j / 2] |= c;
    }

    return (0);
}

static void my_debug(void *ctx, int level, const char *file, int line, const char *str)
{
    ((void)level);

    mbedtls_fprintf((FILE *)ctx, "%s:%04d: %s", file, line, str);
    fflush((FILE *)ctx);
}

static void dump_to_file(const unsigned char *buf, size_t len)
{
    char out_filename[100];
    FILE *out_file;
    printf("--- Time To Dump The Record: \n");
    /* Write packet to file. */
    snprintf(out_filename, sizeof(out_filename), "%s%zd", PACKET_FILE_PREFIX, packet_count);
    out_file = fopen(out_filename, "wb");
    fwrite(buf, sizeof(char), len, out_file);
    fclose(out_file);
    packet_count++;
}

/* Making Random Number Generator to return 1 each time */
static int mbedtls_ctr_drbg_deterministic(void *p_rng, unsigned char *output, size_t output_len)
{
    ((void)p_rng);

    // /* Note that key generation would fail with 0 bytes.
    memset(output, 1, output_len);

    return 0;
}

static int mbedtls_send_custom_server(void *ctx, const unsigned char *buf, size_t len)
{
    int ret = 0;
    ((void)ctx);
    if (symex == 1)
    {
        if (symstep == 4)
        {
            memcpy(shared_buf + (*send_off), &hello_verify_request, len);
            *send_off += len;
            ret = len;
        }
        else if (symstep == 14 && len == SH_unfrag_size)
        {
            if (isFragmented)
            {
                memcpy(shared_buf + (*send_off), &server_hello_frag, SH_frag_size);
                *send_off += SH_frag_size;
                ret = SH_unfrag_size; // We have to trick the stupid server!
            }
            else
            {
                memcpy(shared_buf + (*send_off), &server_hello, len);
                *send_off += len;
                ret = len;
            }
        }
        else if (symstep == 14 && len == SHD_size)
        {
            memcpy(shared_buf + (*send_off), &server_hello_done, len);
            *send_off += len;
            ret = len;
        }
        else if (symstep == 31 && len == CCS_size)
        {
            memcpy(shared_buf + (*send_off), &server_change_cipher, len);
            *send_off += len;
            ret = len;
        }
        else if (symstep == 31 && len == FIN_size)
        {
            memcpy(shared_buf + (*send_off), &server_finished, len);
            *send_off += len;
            ret = len;
        }
    }
    else
    {
        if ((len <= BUF_SIZE) && memcpy(shared_buf + (*send_off), buf, len))
        {
            *send_off += len;
            ret = len;
        }
        else
        {
            ret = -1;
        }
        if (dump_mode == 1)
            dump_to_file(buf, len);
    }

    //////////////////////
    return ret;
}

static int mbedtls_send_custom_client(void *ctx, const unsigned char *buf, size_t len)
{
    int ret = 0;
    ((void)ctx);
    if (symex == 1)
    {
        if (symstep == 1)
        {
            memcpy(shared_buf + (*send_off), &client_hello1, len);
            *send_off += len;
            ret = len;
        }
        else if (symstep == 6)
        {
            memcpy(shared_buf + (*send_off), &client_hello2, len);
            *send_off += len;
            ret = len;
        }

        else if (symstep == 24 && len == CKE_unfrag_size)
        {
            if (isFragmented)
            {
                memcpy(shared_buf + (*send_off), &client_key_exchange_frag, CKE_frag_size);
                *send_off += CKE_frag_size;
                ret = CKE_unfrag_size; // We have to trick the stupid client!
            }
            else
            {
                memcpy(shared_buf + (*send_off), &client_key_exchange, len);
                *send_off += len;
                ret = len;
            }
        }
        else if (symstep == 24 && len == CCS_size)
        {
            memcpy(shared_buf + (*send_off), &client_change_cipher, len);
            *send_off += len;
            ret = len;
        }
        else if (symstep == 24 && len == FIN_size)
        {
            memcpy(shared_buf + (*send_off), &client_finished, len);
            *send_off += len;
            ret = len;
        }
    }
    else
    {
        if ((len <= BUF_SIZE) && memcpy(shared_buf + (*send_off), buf, len))
        {
            *send_off += len;
            ret = len;
        }
        else
        {
            ret = -1;
        }
        if (dump_mode == 1)
            dump_to_file(buf, len);
    }

    //////////////////////
    return ret;
}

static int mbedtls_recv_custom(unsigned char *buf)
{

    int to_recv = (*send_off) - (*recv_off);

    if (to_recv > 0)
    {
        // we assume that to_recv < len

        memcpy(buf, shared_buf + (*recv_off), to_recv);

        (*recv_off) = (*send_off) = 0;
    }

    return to_recv;
}

static int mbedtls_server_send_buf(void *ctx, const unsigned char *buf,
                                   size_t len)
{
    shared_buf = server_send_buf;
    send_off = &server_send_off;

    return mbedtls_send_custom_server(ctx, buf, len);
}

static int mbedtls_client_send_buf(void *ctx, const unsigned char *buf,
                                   size_t len)
{
    shared_buf = client_send_buf;
    send_off = &client_send_off;

    return mbedtls_send_custom_client(ctx, buf, len);
}

static int mbedtls_server_recv_buf(void *ctx, unsigned char *buf,
                                   size_t len)
{
    shared_buf = client_send_buf;
    send_off = &client_send_off;
    recv_off = &server_recv_off;

    return mbedtls_recv_custom(buf);
}

static int mbedtls_client_recv_buf(void *ctx, unsigned char *buf,
                                   size_t len)
{
    shared_buf = server_send_buf;
    send_off = &server_send_off;
    recv_off = &client_recv_off;

    return mbedtls_recv_custom(buf);
}

int mbedtls_s_recv_timeout(void *ctx, unsigned char *buf,
                           size_t len)
{

    return (mbedtls_server_recv_buf(ctx, buf, len));
}

void printusage(char *argv)
{
    puts("\nFor help:");
    printf("Usage: %s [-h]\n\n", argv);
    puts("\nFor Symbolic Constraint Testing:");
    printf("Usage: %s [-i DIR] [-r constraint] [-f] \n\n", argv);
    puts("For Handshake Packet Generation:");
    printf("Usage: %s [-n] \n\n", argv);

    puts("    Constraint:\t\t\t\tDescription:");
    puts("----------------------------------------------------------------------\n");
    puts("[+] The List of Record-level Constraints:\n");   

    puts("[*] ctypevalidity-server\t\tContent Type Validity for the server side");
    puts("[*] ctypevalidity-client\t\tContent Type Validity for the client side");
    puts("[*] recversion-server\t\t\tRecord Version Validity for the server side");
    puts("[*] recversion-client\t\t\tRecord Version Validity for the client side");
    puts("[*] epoch-server\t\t\tEpoch Validity for the server side");
    puts("[*] epoch-client\t\t\tEpoch Validity for the client side");
    puts("[*] rsequniqueness-server\t\tRecord Sequence Uniqueness for the server side");
    puts("[*] rsequniqueness-client\t\tRecord Sequence Uniqueness for the client side");
    puts("[*] rseqwindowing-server\t\tRecord Sequence Windowing for the server side");
    puts("[*] rseqwindowing-client\t\tRecord Sequence Windowing for the client side");    
    puts("[*] rlenvalidity-server\t\t\tRecord Length Validity for the server side");
    puts("[*] rlenvalidity-client\t\t\tRecord Length Validity for the client side");

    puts("\n[+] The List of Fragment-level Constraints:\n");

    puts("[*] htypevalidity-server\t\tHandshake Type Validity for the server side");
    puts("[*] htypevalidity-client\t\tHandshake Type Validity for the client side");
    puts("[*] mlenvalidity-server\t\t\tMessage Length Validity for the server side");
    puts("[*] mlenvalidity-client\t\t\tMessage Length Validity for the client side");
    puts("[*] mseqvalidity-server\t\t\tMessage Sequence Validity for the server side");
    puts("[*] mseqvalidity-client\t\t\tMessage Sequence Validity for the client side");
    puts("[*] flenvalidity-server\t\t\tFragment Length Validity for the server side");
    puts("[*] flenvalidity-client\t\t\tFragment Length Validity for the client side");
    puts("[*] fragoffset-server\t\t\tFragment Offset Validity for the server side");
    puts("[*] fragoffset-client\t\t\tFragment Offset Validity for the client side");
    puts("[*] flenmleneq-server\t\t\tEquality of Fragment Length and Message Length for the server side");
    puts("[*] flenmleneq-client\t\t\tEquality of Fragment Length and Message Length for the client side");
    puts("[*] bytecontain-server\t\t\tIs Fragmentation done Correctly for the server side");
    puts("[*] bytecontain-client\t\t\tIs Fragmentation done Correctly for the client side");
    puts("[*] mseqeq-server\t\t\tMessage Sequence Equality in all Fragments of the same message for the server side");
    puts("[*] mseqeq-client\t\t\tMessage Sequence Equality in all Fragments of the same message for the client side");
    puts("[*] mleneq-server\t\t\tMessage Length Equality in all Fragments of the same message for the server side");
    puts("[*] mleneq-client\t\t\tMessage Length Equality in all Fragments of the same message for the client side");
    
    puts("\n[+] The List of Message-level Constraints:\n");

    puts("[*] handversion-server\t\t\tHandshake Version Validity for the server side");
    puts("[*] handversion-client\t\t\tHandshake Version Validity for the client side");
    puts("[*] cookielenvalidity\t\t\tCookie Length Validity for the server side");
    puts("[*] cookielenvalidity\t\t\tCookie Length Validity for the client side");
    puts("[*] sessionidlenvalidity-server\t\tSession ID Length Validity for the server side");
    puts("[*] sessionidlenvalidity-client\t\tSession ID Length Validity for the client side");
    puts("[*] extlenvalidity-server\t\tExtension Length Validity for the server side");
    puts("[*] extlenvalidity-client\t\tExtension Length Validity for the client side");

    
    
    
    exit(-1);
}

void read_to_buffer_client(int symstep)
{
    if (symstep == 1)
    {
        shared_buf = client_send_buf;
        send_off = &client_send_off;

        memcpy(shared_buf + (*send_off), &client_hello1, CH0_size);
        *send_off += CH0_size;
    }
    else if (symstep == 6)
    {
        shared_buf = client_send_buf;
        send_off = &client_send_off;
        memcpy(shared_buf + (*send_off), &client_hello2, CH2_size);
        *send_off += CH2_size;
    }
    else if (symstep == 24)
    {
        shared_buf = client_send_buf;
        send_off = &client_send_off;
        if (isFragmented)
        {
            memcpy(shared_buf + (*send_off), &client_key_exchange_frag, CKE_frag_size);
            *send_off += CKE_frag_size;
        }
        else
        {
            memcpy(shared_buf + (*send_off), &client_key_exchange, CKE_unfrag_size);
            *send_off += CKE_unfrag_size;
        }

        memcpy(shared_buf + (*send_off), &client_change_cipher, CCS_size);
        *send_off += CCS_size;
        memcpy(shared_buf + (*send_off), &client_finished, FIN_size);
        *send_off += FIN_size;
    }
}

void read_to_buffer_server(int symstep)
{
    if (symstep == 4)
    {
        shared_buf = server_send_buf;
        send_off = &server_send_off;

        memcpy(shared_buf + (*send_off), &hello_verify_request, HVR_size);
        *send_off += HVR_size;
    }
    else if (symstep == 14)
    {
        shared_buf = server_send_buf;
        send_off = &server_send_off;
        if (isFragmented)
        {
            memcpy(shared_buf + (*send_off), &server_hello_frag, SH_frag_size);
            *send_off += SH_frag_size;
        }
        else
        {
            memcpy(shared_buf + (*send_off), &server_hello, SH_unfrag_size);
            *send_off += SH_unfrag_size;
        }

        memcpy(shared_buf + (*send_off), &server_hello_done, SHD_size);
        *send_off += SHD_size;
    }
    else if (symstep == 31)
    {
        shared_buf = server_send_buf;
        send_off = &server_send_off;
        memcpy(shared_buf + (*send_off), &server_change_cipher, CCS_size);
        *send_off += CCS_size;
        memcpy(shared_buf + (*send_off), &server_finished, FIN_size);
        *send_off += FIN_size;
    }
}
int perform_handshake(mbedtls_ssl_context *c_ssl, mbedtls_ssl_context *s_ssl)
{
    int ret = 0;
    do
    {
        int i;
        int no_steps;

        if (c_ssl->state == MBEDTLS_SSL_HANDSHAKE_OVER)
        {
            no_steps = 0;
        }
        else
        {
            no_steps = client_steps[step - 1];
        }

        for (i = 0; i < no_steps; i++)
        {
            if ((experimenttype == CLIENT) || (approach == None))
            {
                ret = mbedtls_ssl_handshake_step(c_ssl);
            }
            else
            {
                read_to_buffer_client(symstep);
            }
            symstep += 1;

            if (ret != 0)
            {
                if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
                {
                    mbedtls_printf(" failed\n  ! mbedtls_ssl_handshake returned -0x%x\n\n", -ret);
                    goto exit;
                }
            }
        }

        mbedtls_printf("--- client handshake step %zd ok\n", step);

        if (s_ssl->state == MBEDTLS_SSL_HANDSHAKE_OVER)
        {
            no_steps = 0;
        }
        else
        {
            no_steps = server_steps[step - 1];
        }

        for (i = 0; i < no_steps; i++)
        {

            if ((experimenttype == SERVER) || (approach == None))
            {
                ret = mbedtls_ssl_handshake_step(s_ssl);
            }
            else
            {
                read_to_buffer_server(symstep);
            }
            symstep += 1;

            if (ret != 0)
            {
                if (ret == MBEDTLS_ERR_SSL_HELLO_VERIFY_REQUIRED)
                {
                    printf(" hello verification requested\n");
                    ret = 0;
                    mbedtls_ssl_session_reset(s_ssl);
                    server_recv_off = 0;
                }
                else if (ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE)
                {
                    mbedtls_printf(" failed\n  ! mbedtls_ssl_handshake returned %d\n\n", ret);
                    goto exit;
                }
            }
        }

        mbedtls_printf("--- server handshake step %zd ok\n", step);

        step++;
    } while (((c_ssl->state != MBEDTLS_SSL_HANDSHAKE_OVER) || (s_ssl->state != MBEDTLS_SSL_HANDSHAKE_OVER)) && (step <= MAX_HANDSHAKE_STEPS));

exit:
    return (ret);
}

int load_psk_records_from_file(char *rule)
{
    char file_name[100];
    ////////////////////////////////// Load First Client Hello
    sprintf(file_name, "%s/%s", base_name, "CH0");
    load_record(file_name, CH0_buf, CH0_size);
    memcpy(shadow_CH0, CH0_buf, CH0_size);
    printf("CH0 Loaded Successfully.\n");
    ////////////////////////////////// Load Hello Verify Request
    sprintf(file_name, "%s/%s", base_name, "HVR");
    load_record(file_name, HVR_buf, HVR_size);
    memcpy(shadow_HVR, HVR_buf, HVR_size);
    printf("HVR Loaded Successfully.\n");
    ///////////////////////////////// Load Client Hello with Cookie
    sprintf(file_name, "%s/%s", base_name, "CH2");
    load_record(file_name, CH2_buf, CH2_size);
    memcpy(shadow_CH2, CH2_buf, CH2_size);
    printf("CH2 Loaded Successfully.\n");

    //////////////////////////////// Load Server Hello Done
    sprintf(file_name, "%s/%s", base_name, "SHD");
    load_record(file_name, SHD_buf, SHD_size);
    memcpy(shadow_SHD, SHD_buf, SHD_size);
    printf("SHD Loaded Successfully.\n");
    //////////////////////////////// Load Client Key Exchange
    if (isFragmented)
    {
        sprintf(file_name, "%s/%s", base_name, "CKE");
        load_record(file_name, CKE_buffrag, CKE_frag_size);
        memcpy(shadow_CKE_buffrag, CKE_buffrag, CKE_frag_size);
        printf("CKE Fragmented Loaded Successfully.\n");

        ///////////////////////////////// Load Server Hello
        sprintf(file_name, "%s/%s", base_name, "SH");
        load_record(file_name, SH_buffrag, SH_frag_size);
        memcpy(shadow_SH_buffrag, SH_buffrag, SH_frag_size);
        printf("SH Fragmented Loaded Successfully.\n");
    }
    else
    {
        sprintf(file_name, "%s/%s", base_name, "CKE");
        load_record(file_name, CKE_buf, CKE_unfrag_size);
        memcpy(shadow_CKE, CKE_buf, CKE_unfrag_size);
        printf("CKE Loaded Successfully.\n");

        ///////////////////////////////// Load Server Hello
        sprintf(file_name, "%s/%s", base_name, "SH");
        load_record(file_name, SH_buf, SH_unfrag_size);
        memcpy(shadow_SH, SH_buf, SH_unfrag_size);
        printf("SH Loaded Successfully.\n");
    }

    /////////////////////////////// Load Client Change Cipher Suite
    sprintf(file_name, "%s/%s", base_name, "CCC");
    load_record(file_name, CCC_buf, CCS_size);
    memcpy(shadow_CCC, CCC_buf, CCS_size);
    printf("CCC Loaded Successfully.\n");
    /////////////////////////////// Load Client Finished
    sprintf(file_name, "%s/%s", base_name, "CFI");
    load_record(file_name, CFI_buf, FIN_size);
    memcpy(shadow_CFI, CFI_buf, FIN_size);
    printf("CFI Loaded Successfully.\n");
    /////////////////////////////// Load Server Change Cipher Suite
    sprintf(file_name, "%s/%s", base_name, "SCC");
    load_record(file_name, SCC_buf, CCS_size);
    memcpy(shadow_SCC, SCC_buf, CCS_size);
    printf("SCC Loaded Successfully.\n");
    /////////////////////////////// Load Server Finished
    sprintf(file_name, "%s/%s", base_name, "SFI");
    load_record(file_name, SFI_buf, FIN_size);
    memcpy(shadow_SFI, SFI_buf, FIN_size);
    printf("SFI Loaded Successfully.\n");
    ////////////////////////////////////////////////////
    parse_record(CH0_buf, &client_hello1);
    shadow_client_hello1 = client_hello1;

    parse_record(CH2_buf, &client_hello2);
    shadow_client_hello2 = client_hello2;
    if (isFragmented)
    {
        parse_record(CKE_buffrag, &client_key_exchange_frag);
        shadow_client_key_exchange_frag = client_key_exchange_frag;

        parse_record(SH_buffrag, &server_hello_frag);
        shadow_server_hello_frag = server_hello_frag;
    }
    else
    {
        parse_record(CKE_buf, &client_key_exchange);
        shadow_client_key_exchange = client_key_exchange;

        parse_record(SH_buf, &server_hello);
        shadow_server_hello = server_hello;
    }

    parse_record(CCC_buf, &client_change_cipher);
    shadow_client_change_cipher = client_change_cipher;

    parse_record(CFI_buf, &client_finished);
    shadow_client_finished = client_finished;
    //
    parse_record(HVR_buf, &hello_verify_request);
    shadow_hello_verify_request = hello_verify_request;

    parse_record(SHD_buf, &server_hello_done);
    shadow_server_hello_done = server_hello_done;

    parse_record(SCC_buf, &server_change_cipher);
    shadow_server_change_cipher = server_change_cipher;

    parse_record(SFI_buf, &server_finished);
    shadow_server_finished = server_finished;

    ////////////////////////////////////////////////////
#if ISSYM

    //////////////////// Record-Level Constraints

    if (strcmp(rule, "ctypevalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("ContentType Server Rule:\n\n");
        contentTypeServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "ctypevalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("ContentType Client Rule:\n\n");
        contentTypeClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "recversion-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Record Version Server Rule:\n\n");
        recordVersionServer(&client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "recversion-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Record Version Client Rule:\n\n");
        recordVersionClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "epoch-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Epoch Rule for the Server Side:\n\n");
        epochServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "epoch-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Epoch Rule for the Client Side:\n\n");
        epochClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "rsequniqueness-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Record Sequence Number Rule 2 for the Server Side:\n\n");
        sequenceUniquenessServer(&client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "rsequniqueness-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Record Sequence Number Rule 2 for the Client Side:\n\n");
        sequenceUniquenessClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "rseqwindowing-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Record Sequence Number Rule 3 for the Server Side:\n\n");
        sequenceWindowServer(&client_hello2, &client_key_exchange, &client_change_cipher);
    }
    else if (strcmp(rule, "rseqwindowing-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Record Sequence Number Rule 3 for the Client Side:\n\n");
        sequenceWindowClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher);
    }
    else if (strcmp(rule, "rlenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Record Length Validity Server Rule:\n\n");
        recordLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher,
                                   &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange, &shadow_client_change_cipher);
    }
    else if (strcmp(rule, "rlenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Record Length Validity Client Rule:\n\n");
        recordLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher,
                                   &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done, &shadow_server_change_cipher);
    }
    ///////////////////////// Fragment-Level Constraints
    
    else if (strcmp(rule, "htypevalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Handshake Type Server Rule:\n\n");
        handshakeTypeValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
        &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
    }

    else if (strcmp(rule, "htypevalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Handshake Type Client Rule:\n\n");
        handshakeTypeValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
        &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
    }

    else if (strcmp(rule, "mlenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Message Length Validity Server Rule:\n\n");
        messageLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
                                    &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
    }
    else if (strcmp(rule, "mlenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Message Length Validity Client Rule:\n\n");
        messageLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
                                    &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
    }
    else if (strcmp(rule, "mseqvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Message Sequence Number Rule for the Server Side:\n\n");
        messageSequenceServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "mseqvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Message Sequence Number Rule for the Client Side:\n\n");
        messageSequenceClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "flenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Fragment Length Validity Server Rule:\n\n");
        fragmentLengthValidityServer(&client_hello1, &client_hello2, &client_key_exchange,
                                     &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange);
    }
    else if (strcmp(rule, "flenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Fragment Length Validity Client Rule:\n\n");
        fragmentLengthValidityClient(&hello_verify_request, &server_hello, &server_hello_done,
                                     &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done);
    }
    else if (strcmp(rule, "fragoffset-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragment Offset Validity Server Rule:\n");
        offsetValidityNoFragServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "fragoffset-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragment Offset Validity Client Rule:\n");
        offsetValidityNoFragClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "flenmleneq-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragment Length Message Length Equality Server Rule:\n");
        fragLenMessageLenEqualityServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "flenmleneq-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragment Length Message Length Equality Client Rule:\n");
        fragLenMessageLenEqualityClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "bytecontain-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Byte contained Server:\n");
        byteContainedFragServer(&client_key_exchange_frag);
    }
    else if (strcmp(rule, "bytecontain-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Byte contained Client:\n");
        byteContainedFragClient(&server_hello_frag);
    }
    else if (strcmp(rule, "mseqeq-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Message Sequence Equality In Both Fragments Server:\n");
        messageSequenceEqualityFragServer(&client_key_exchange_frag);
    }
    else if (strcmp(rule, "mseqeq-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Message Sequence Equality In Both Fragments Client:\n");
        messageSequenceEqualityFragClient(&server_hello_frag);
    }
    else if (strcmp(rule, "mleneq-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Message Length Equality In Both Fragments Server:\n");
        MessageLengthFragEqualityServer(&client_key_exchange_frag);
    }
    else if (strcmp(rule, "mleneq-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Message Length Equality In Both Fragments Client:\n");
        MessageLengthFragEqualityClient(&server_hello_frag);
    }    
    /////////////////////// Message-Level Constraints
    else if (strcmp(rule, "handversion-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Handshake Version Server Rule:\n\n");
        handshakeVersionServer(&client_hello1, &client_hello2);
    }
    else if (strcmp(rule, "handversion-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Handshake Version Client Rule:\n\n");
        handshakeVersionClient(&hello_verify_request, &server_hello);
    }
    
    else if (strcmp(rule, "cookielenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Cookie Length Rule Server:\n\n");
        cookieLengthServer(&client_hello1, &client_hello2, &shadow_client_hello1,
                           &shadow_client_hello2);
    }
    else if (strcmp(rule, "cookielenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Cookie Length Rule Client:\n\n");
        cookieLengthClient(&hello_verify_request, &shadow_hello_verify_request);
    }
    else if (strcmp(rule, "sessionidlenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("SessionID Rule Server:\n\n");
        sessionIDLengthServer(&client_hello1, &client_hello2, &shadow_client_hello1,
                              &shadow_client_hello2);
    }
    else if (strcmp(rule, "sessionidlenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("SessionID Rule Client:\n\n");
        sessionIDLengthClient(&server_hello, &shadow_server_hello);
    }
    else if (strcmp(rule, "extlenvalidity-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Extension Length Validity Server:\n\n");
        extensionLengthValidityServer(&client_hello1, &client_hello2, &shadow_client_hello1,
        &shadow_client_hello2);
    }
    else if (strcmp(rule, "extlenvalidity-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Extension Length Validity client:\n\n");
        extensionLengthValidityClient(&server_hello, &shadow_server_hello);
    }

    /*
    else if (strcmp(rule, "rule7-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Fragment Length Record Length Relation Server Rule:\n\n");
        fragmentLenRecordLenServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "rule7-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Fragment Length Record Length Relation Client Rule:\n\n");
        fragmentLenRecordLenClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "rule8-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Message Length Record Length Relation Server Rule:\n\n");
        messageLenRecordlenServer(&client_hello1, &client_hello2, &client_key_exchange);
    }
    else if (strcmp(rule, "rule8-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Message Length Record Length Relation Client Rule:\n\n");
        messageLenRecordlenClient(&hello_verify_request, &server_hello, &server_hello_done);
    }
    else if (strcmp(rule, "rule17-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Fragment Offset Validity Server:\n");
        offsetValidityFragServer(&client_key_exchange_frag, &shadow_client_key_exchange_frag);
    }
    else if (strcmp(rule, "rule17-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Fragment Offset Validity Client:\n");
        offsetValidityFragClient(&server_hello_frag, &shadow_server_hello_frag);
    }
    
    else if (strcmp(rule, "rule21-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Fragment Length Message Length Relation Server:\n");
        fragLenMessageLenRelationServer(&client_key_exchange_frag);
    }
    else if (strcmp(rule, "rule21-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        puts("Fragmentation Constraints:");
        puts("Fragmentat Length Message Length Relation Client:\n");
        fragLenMessageLenRelationClient(&server_hello_frag);
    }
    else if (strcmp(rule, "all-server") == 0)
    {
        experimenttype = SERVER;
        approach = CONFORMANCE;
        printf("Performing all experiments simultaneously Server-Side:\n\n\n\n");
        allConstraintsServer(&client_hello1, &client_hello2, &client_key_exchange, &client_change_cipher,
                             &shadow_client_hello1, &shadow_client_hello2, &shadow_client_key_exchange, &shadow_client_change_cipher);
    }
    else if (strcmp(rule, "all-client") == 0)
    {
        experimenttype = CLIENT;
        approach = CONFORMANCE;
        printf("Performing all experiments simultaneously Client-Side:\n\n\n\n");
        allConstraintsClient(&hello_verify_request, &server_hello, &server_hello_done, &server_change_cipher,
                             &shadow_hello_verify_request, &shadow_server_hello, &shadow_server_hello_done, &shadow_server_change_cipher);
    }
    */

    ///////////////// StatefulSymbolic Execution 
    else if (strcmp(rule, "CH0") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        printf("Symbolically Executing ClientHello:\n\n");
        symbolicClientHello1(&client_hello1, &shadow_client_hello1);
    }
    else if (strcmp(rule, "CH2") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        printf("Symbolically Executing ClientHello2:\n\n");
        symbolicClientHello2(&client_hello2, &shadow_client_hello2);
    }
    else if (strcmp(rule, "CKE") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        printf("Symbolically Executing ClientKeyExchange:\n\n");
        symbolicClientKeyExchange(&client_key_exchange, &shadow_client_key_exchange);
    }
    else if (strcmp(rule, "CCC") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        printf("Symbolically Executing ChangeCipherSuite(S):\n\n");
        symbolicClientChangeCipher(&client_change_cipher, &shadow_client_change_cipher);
    }
    else if (strcmp(rule, "CFI") == 0)
    {
        experimenttype = SERVER;
        approach = SYMBEX;
        printf("Symbolically Executing Finished(S):\n\n");
        symbolicClientFinished(&client_finished, &shadow_client_finished);
    }
    else if (strcmp(rule, "HVR") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        printf("Symbolically Executing HelloVerifyRequest:\n\n");
        symbolicHelloVerifyRequest(&hello_verify_request, &shadow_hello_verify_request);
    }
    else if (strcmp(rule, "SH") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        printf("Symbolically Executing ServerHello:\n\n");
        symbolicServerHello(&server_hello, &shadow_server_hello);
    }
    else if (strcmp(rule, "SHD") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        printf("Symbolically Executing ServerHelloDone:\n\n");
        symbolicServerHelloDone(&server_hello_done, &shadow_server_hello_done);
    }
    else if (strcmp(rule, "SCC") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        printf("Symbolically Executing ChangeCipherSuite(C):\n\n");
        symbolicServerChangeCipher(&server_change_cipher, &shadow_server_change_cipher);
    }
    else if (strcmp(rule, "SFI") == 0)
    {
        experimenttype = CLIENT;
        approach = SYMBEX;
        printf("Symbolically Executing Finished(C):\n\n");
        symbolicServerFinished(&server_finished, &shadow_server_finished);
    }
    else if (strcmp(rule, "None") == 0)
    {
        approach = None;
        printf("Normal Execution!\n\n");
    }
    else
    {
        approach = None;
        printf("Rule is not defined!\n\n");
        printusage("./dtls-fuzz");
    }

#endif

    return 0;
}
int main(int argc, char *argv[])
{

    int opt;      // Parsing program arguments
    int iarg = 0; //input directory argument
    int rarg = 0; //Rule argument
    int harg = 0;
    int narg = 0; //Normal or Symbolic execution Argument

    ////////////////////////////////////////////////
    ////////////////////////////////////////////////

    int ret;

    mbedtls_ssl_context s_ssl; // Context of server
    mbedtls_ssl_context c_ssl; // Context of client
    mbedtls_ssl_config s_conf; //Config of server
    mbedtls_ssl_config c_conf; //Config of Client

    int force_ciphersuite[2];
    force_ciphersuite[0] = mbedtls_ssl_get_ciphersuite_id("TLS-PSK-WITH-AES-128-CCM");
    force_ciphersuite[1] = 0;
    unsigned char psk[MBEDTLS_PSK_MAX_LEN];
    size_t psk_len = 0;
    char psk_value[] = "1234";
    char psk_id[] = "Client_identity";

    //////////////
    mbedtls_entropy_context s_entropy;
    mbedtls_ctr_drbg_context s_ctr_drbg;
    mbedtls_x509_crt srvcert; //Server Certificate

    mbedtls_pk_context pkey; // Public key information and Context
    mbedtls_ssl_cache_context cache;
    mbedtls_ssl_cookie_ctx cookie_ctx; //Cookie Context
    mbedtls_timing_delay_context s_timer;
    mbedtls_x509_crt caservercert; // CA Certificate

    mbedtls_x509_crt_init(&caservercert);
    mbedtls_debug_set_threshold(DEBUG_LEVEL);
    mbedtls_entropy_init(&s_entropy);
    mbedtls_ctr_drbg_init(&s_ctr_drbg);
    mbedtls_x509_crt_init(&srvcert);
    mbedtls_pk_init(&pkey); // Initializing Public key information and Context
    mbedtls_ssl_cache_init(&cache);
    mbedtls_ssl_cookie_init(&cookie_ctx); //Initializing Cookie
    mbedtls_ssl_init(&s_ssl);             //Initializng server Context
    mbedtls_ssl_config_init(&s_conf);     //Initializing Server Config

    if ((ret = mbedtls_ctr_drbg_seed(&s_ctr_drbg, mbedtls_entropy_func,
                                     &s_entropy, (const unsigned char *)pers,
                                     strlen(pers))) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ctr_drbg_seed returned -0x%x\n",
                       (unsigned int)-ret);
        goto exit;
    }

    if ((ret = mbedtls_ssl_config_defaults(&s_conf,
                                           MBEDTLS_SSL_IS_SERVER,
                                           MBEDTLS_SSL_TRANSPORT_DATAGRAM,
                                           MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ssl_config_defaults returned %d\n\n", ret);
        goto exit;
    }

    mbedtls_ssl_conf_rng(&s_conf, mbedtls_ctr_drbg_deterministic, &s_ctr_drbg);
    mbedtls_ssl_conf_dbg(&s_conf, my_debug, stdout);

    mbedtls_ssl_conf_session_cache(&s_conf, &cache,
                                   mbedtls_ssl_cache_get,
                                   mbedtls_ssl_cache_set);

    /* Initializing Cookie */
    if ((ret = mbedtls_ssl_cookie_setup(&cookie_ctx, mbedtls_ctr_drbg_deterministic, &s_ctr_drbg)) != 0)
    {
        printf(" failed\n  ! mbedtls_ssl_cookie_setup returned %d\n\n", ret);
        goto exit;
    }
    mbedtls_ssl_conf_dtls_cookies(&s_conf, mbedtls_ssl_cookie_write, mbedtls_ssl_cookie_check,
                                  &cookie_ctx);

    mbedtls_ssl_conf_ciphersuites(&s_conf, force_ciphersuite); // Force Cipher Suite

    if (unhexify(psk, psk_value, &psk_len) != 0)
    {
        mbedtls_printf("pre-shared key not valid hex\n");
        goto exit;
    }

    ret = mbedtls_ssl_conf_psk(&s_conf, psk, psk_len,
                               (const unsigned char *)psk_id,
                               strlen(psk_id));
    if (ret != 0)
    {
        mbedtls_printf("  failed\n  mbedtls_ssl_conf_psk returned -0x%04X\n\n", (unsigned int)-ret);
        goto exit;
    }

    //mbedtls_ssl_conf_min_version(&s_conf, MBEDTLS_SSL_MAJOR_VERSION_3, 3);
    //mbedtls_ssl_conf_max_version(&s_conf, MBEDTLS_SSL_MAJOR_VERSION_3, 3);
    /* Finalizing Server Setup */
    if ((ret = mbedtls_ssl_setup(&s_ssl, &s_conf)) != 0)
    {
        printf(" failed\n  ! mbedtls_ssl_setup returned %d\n\n", ret);
        goto exit;
    }

    mbedtls_ssl_set_timer_cb(&s_ssl, &s_timer, mbedtls_timing_set_delay, mbedtls_timing_get_delay);
    mbedtls_ssl_set_datagram_packing(&s_ssl, 0); // disable datagram packing for server

    mbedtls_ssl_session_reset(&s_ssl);
    // mbedtls_net_recv_timeout instead of the last NULL in Dtls_server
    mbedtls_ssl_set_bio(&s_ssl, NULL, mbedtls_server_send_buf, mbedtls_server_recv_buf, NULL);

    //////////////////////////////////////////////////////// Client Stuffs

    mbedtls_entropy_context c_entropy;
    mbedtls_ctr_drbg_context c_ctr_drbg;
    mbedtls_timing_delay_context c_timer;
    mbedtls_x509_crt cacert;
    mbedtls_x509_crt clicert;
    mbedtls_pk_context c_pkey;
    mbedtls_ssl_init(&c_ssl);         // Initialization of Client Context
    mbedtls_ssl_config_init(&c_conf); //Initialization of Client Config
    mbedtls_x509_crt_init(&cacert);
    mbedtls_x509_crt_init(&clicert);
    mbedtls_ctr_drbg_init(&c_ctr_drbg);
    mbedtls_entropy_init(&c_entropy);
    mbedtls_ssl_init(&c_ssl);         // Initialization of Client Context
    mbedtls_ssl_config_init(&c_conf); //Initialization of Client Config
    mbedtls_pk_init(&c_pkey);

    /* Set Client Configurations  */
    if ((ret = mbedtls_ssl_config_defaults(&c_conf,
                                           MBEDTLS_SSL_IS_CLIENT,
                                           MBEDTLS_SSL_TRANSPORT_DATAGRAM,
                                           MBEDTLS_SSL_PRESET_DEFAULT)) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ssl_config_defaults returned %d\n\n", ret);
        goto exit;
    }

    /* Setting the Client context to use deterministic Random Number Generator */
    mbedtls_ssl_conf_rng(&c_conf, mbedtls_ctr_drbg_deterministic, &c_ctr_drbg);
    mbedtls_ssl_conf_dbg(&c_conf, my_debug, stdout);

    mbedtls_ssl_conf_ciphersuites(&c_conf, force_ciphersuite); // Force Cipher Suite

    ret = mbedtls_ssl_conf_psk(&c_conf, psk, psk_len,
                               (const unsigned char *)psk_id,
                               strlen(psk_id));
    if (ret != 0)
    {
        mbedtls_printf("  failed\n  mbedtls_ssl_conf_psk returned -0x%04X\n\n", (unsigned int)-ret);
        goto exit;
    }

    //mbedtls_ssl_conf_min_version(&c_conf, MBEDTLS_SSL_MAJOR_VERSION_3, 3);
    //mbedtls_ssl_conf_max_version(&c_conf, MBEDTLS_SSL_MAJOR_VERSION_3, 3);
    /* Finalizing Client Setup */
    if ((ret = mbedtls_ssl_setup(&c_ssl, &c_conf)) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ssl_setup returned %d\n\n", ret);
        goto exit;
    }
    if ((ret = mbedtls_ssl_set_hostname(&c_ssl, "DTLS Server")) != 0)
    {
        mbedtls_printf(" failed\n  ! mbedtls_ssl_set_hostname returned %d\n\n", ret);
        goto exit;
    }

    mbedtls_ssl_set_datagram_packing(&c_ssl, 0); // disable datagram packing for Client

    // I put the last argument NULL again instead of setting it: mbedtls_net_recv_timeout
    mbedtls_ssl_set_bio(&c_ssl, NULL, mbedtls_client_send_buf, mbedtls_client_recv_buf, NULL);

    mbedtls_ssl_set_timer_cb(&c_ssl, &c_timer, mbedtls_timing_set_delay, mbedtls_timing_get_delay);

    ////////////////////////////////////////////////
    ////////////////////////////////////////////////
    while ((opt = getopt(argc, argv, "i:r:nfh")) != -1)
    {
        switch (opt)
        {
        case 'i':
            iarg = 1;
            if (sizeof(optarg) < 100)
                strcpy(base_name, optarg);
            // strcpy(base_name, "handshakes/nofrag");
            else
            {
                printf("Size of optarg exceeds the normal size\n");
                printusage(argv[0]);
            }
            break;

        case 'r':
            rarg = 1;
            strcpy(rule, optarg);
            // strcpy(rule, "None");
            break;

        case 'n':
            narg = 1;
            break;
        case 'f':
            isFragmented = 1;
            break;
        case 'h':
            harg = 1;
            break;
        default:
            printusage(argv[0]);
            break;
        }
    }

    if (iarg == 1 && rarg == 1 && narg == 0 && harg == 0)
    {
        if (((strcmp(rule, "bytecontain-server") == 0) || (strcmp(rule, "bytecontain-client") == 0) ||
             (strcmp(rule, "mseqeq-server") == 0) || (strcmp(rule, "mseqeq-client") == 0) ||
             (strcmp(rule, "mleneq-server") == 0) || (strcmp(rule, "mleneq-client") == 0)) &&
             (isFragmented == 0))
        {
            puts("Fragmentation should be enabled with this constraint!\n");
            puts("[-] Add -f to proceed!\n");
            printusage(argv[0]);
        }

        else if (((strcmp(rule, "bytecontain-server") != 0) && (strcmp(rule, "bytecontain-client") != 0) &&
                  (strcmp(rule, "mseqeq-server") != 0) && (strcmp(rule, "mseqeq-client") != 0) &&
                  (strcmp(rule, "mleneq-server") != 0) && (strcmp(rule, "mleneq-client") != 0)) &&
                  (isFragmented == 1))
        {
            puts("Fragmentation cannot be enabled with this constraint!\n");
            puts("[-] Remove -f to proceed!\n");
            printusage(argv[0]);
        }
        dump_mode = 0;
        symex = 1;

        if ((ret = load_psk_records_from_file(rule)) != 0)
        {
            goto exit;
        }

        perform_handshake(&c_ssl, &s_ssl);
    }
    else if (iarg == 0 && rarg == 0 && narg == 1 && isFragmented == 0 && harg == 0)
    {
        dump_mode = 1;
        perform_handshake(&c_ssl, &s_ssl);
    }
    else
    {
        printusage(argv[0]);
    }

exit:
    return (ret);
}
