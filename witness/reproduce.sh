#!/bin/bash
readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

function setup_dtlsfuzzer() {
    echo "setup of dtls-fuzzer to be implemented"
}

setup_dtlsfuzzer

for test_file in $SCRIPT_DIR/*/*/dtls-fuzzer.test; do
    echo "Running test $test_file"
    java -Dmbedtls.version=2.26.0 -jar target/dtls-fuzzer.jar dtls-fuzzer.args -test $test_file
done
